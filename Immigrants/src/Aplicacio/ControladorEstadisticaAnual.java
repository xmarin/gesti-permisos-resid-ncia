package Aplicacio;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import Persistencia.BBDDEstadistica;
import Persistencia.BBDDNacio;

public class ControladorEstadisticaAnual {
	private BBDDNacio bdNacio;
	private BBDDEstadistica bdEstadistica;
	
	public ControladorEstadisticaAnual() throws Exception{
		bdNacio = new BBDDNacio();
		bdEstadistica = new BBDDEstadistica();
	}
	
	public String getEstadisticaAnual(String any) throws Exception{
		if (any.trim().length() != 4) throw new Exception("Format any incorrecte. L'any ha de tenir 4 d�gits. Ex: 2011");
		
		int anyEstadistica;
		int anyActual;
		
		try{
			anyEstadistica = Integer.parseInt(any);
			anyActual = Calendar.getInstance().get(Calendar.YEAR);
		}catch (Exception e){
			throw new Exception("Format any incorrecte. Nom�s s'admeten n�meros. Ex: 2010");
		}
		
		if (anyEstadistica >= anyActual) throw new Exception("L'any per generar l'estad�stica ha de ser any tancat.");
		Calendar c2 = new GregorianCalendar();
		SimpleDateFormat sdf2 = new SimpleDateFormat("yy-MM-dd hh:mm:ss");
		
		return "s";
	}
	
	private void getPermisosAnyTancat(String any) throws Exception{
		if (any.trim().isEmpty()) throw new Exception("L'any no pot estar buit.");
		bdEstadistica.getPermisosAnyTancat(any);
	}
}
