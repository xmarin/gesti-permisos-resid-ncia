package Aplicacio;

import java.util.HashMap;

import Domini.Amfitrio;
import Persistencia.BBDDAmfitrio;

public class ControladorHistorialAmfitrio {
	private BBDDAmfitrio conAmfitrio;
	private Amfitrio amfitrio;
	
	public ControladorHistorialAmfitrio() throws Exception{
		conAmfitrio = new BBDDAmfitrio();
	}
	
	public String[][] searchAmfitrio(String nifOrName) throws Exception{
		HashMap<String, Amfitrio> HashMapAmfitrio = new HashMap<String, Amfitrio>();

		HashMapAmfitrio = conAmfitrio.searchAmfitrio(nifOrName.toUpperCase()); 
		if (HashMapAmfitrio == null){ 
			return null;             
		}else{ 
			String res[][] = new String[HashMapAmfitrio.size()][4]; 
			int i = 0; 
			for (Amfitrio amf : HashMapAmfitrio.values()){
				res[i][0] = amf.getNif();
				res[i][1] = amf.getFiliacio(); 
				res[i][2] = amf.getDomicili();  
				res[i][3] = amf.getPoblacio();     
				i++;                  
			} 
		return res;
		}       
	}
	
	public void carregaAmfitrio(String NIF) throws Exception{
		amfitrio = conAmfitrio.find(NIF.toUpperCase());
		if (amfitrio == null) throw new Exception("L'amfitri� "+NIF+" no esta donat d'alta al sistema.");
	}
	
	public String getFiliacio(){
		return amfitrio.getFiliacio();
	}
	
	public String getDomicili(){
		return amfitrio.getDomicili();
	}
	
	public String getPoblacio(){
		return amfitrio.getPoblacio();
	}
	
	public String getNif(){
		return amfitrio.getNif();
	}

	public String[][] getHistorialAmf() throws Exception {
		HashMap<String, String[]> HashMapHistorialAmf = new HashMap<String, String[]>();

		HashMapHistorialAmf = conAmfitrio.getHistorialAmfitrio(this.getNif()); 
		
		if (HashMapHistorialAmf == null){ 
			return null;             
		}else{ 
			String res[][] = new String[HashMapHistorialAmf.size()][6]; 
			int i = 0; 
			for (String[] dada : HashMapHistorialAmf.values()){
				res[i][0] = dada[0];
				res[i][1] = dada[1]; 
				res[i][2] = dada[2];  
				res[i][3] = dada[3];  
				res[i][4] = dada[4];  
				res[i][5] = dada[5];  
				i++;                  
			} 	

		return res;
		}  
	}
}
