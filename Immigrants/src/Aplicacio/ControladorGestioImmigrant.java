package Aplicacio;

import java.util.HashMap;

import Aplicacio.Representations.ImmigrantRepresentation;
import Domini.Immigrant;
import Domini.Nacio;
import Persistencia.BBDDImmigrant;
import Persistencia.BBDDNacio;

public class ControladorGestioImmigrant {
	private BBDDNacio conNacions;
	private BBDDImmigrant conImmigrant;
	private HashMap<String,Nacio> nacions;
	private Immigrant imm;
	
	public ControladorGestioImmigrant() throws Exception{
		conNacions = new BBDDNacio();
		conImmigrant = new BBDDImmigrant();
	}
	
	public HashMap<String,String> getNacions() throws Exception{
		nacions = conNacions.findNacio();
		HashMap<String,String> res = new HashMap<String,String>();

		for (String s : nacions.keySet()){
			res.put(s, nacions.get(s).toString());
		}
		return res;
	}
	
	public void afegirImmigrant(String passaport, String codiNacio ,String filiacio, String domicili, String poblacio) throws Exception {
		if (conImmigrant.find(passaport.toUpperCase()) != null) {
			throw new Exception("L'immigrant amb passaport " +passaport+ " ja existeix.");
		}
		
		imm = new Immigrant(passaport, nacions.get(codiNacio), filiacio, domicili, poblacio);
		conImmigrant.inserirImmigrant(imm);
	}
	
	public Immigrant carregaImmigrant(String passport) throws Exception{
		//Mirar si imm es el correcte
		imm = conImmigrant.find(passport.toUpperCase());
		
		if (imm == null) throw new Exception("L'immigrant " +passport+" no esta donat d'alta al sistema.");
		
		return imm;
	}

	public void modificarImmigrant(String codiNacio ,String filiacio, String domicili, String poblacio) throws Exception{
		imm.setDomicili(domicili);
		imm.setFiliacio(filiacio);
		imm.setNacio(nacions.get(codiNacio));
		imm.setPoblacio(poblacio);
		
		conImmigrant.modificarImmigrant(imm);
	}
	
	public ImmigrantRepresentation getImmigrantRepresentation(String passaport) throws Exception {
		carregaImmigrant(passaport);
		return new ImmigrantRepresentation(imm); 
	}
}
