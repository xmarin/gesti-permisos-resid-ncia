package Aplicacio;


import java.util.HashMap;

import Domini.Immigrant;
import Persistencia.BBDDImmigrant;

public class ControladorHistorialImmigrant {
	private BBDDImmigrant conImmigrant;
	private Immigrant immigrant;
	
	public ControladorHistorialImmigrant() throws Exception{
		conImmigrant = new BBDDImmigrant();
	}
	
	public String[][] searchImmigrant(String passaportOrName) throws Exception{
		HashMap<String, Immigrant> HashMapImmigrant = new HashMap<String, Immigrant>();

		HashMapImmigrant = conImmigrant.searchImmigrants(passaportOrName.toUpperCase()); 
		if (HashMapImmigrant == null){ 
			return null;             
		}else{ 
			String res[][] = new String[HashMapImmigrant.size()][5]; 
			int i = 0; 
			for (Immigrant imm : HashMapImmigrant.values()){
				res[i][0] = imm.getPassaport();
				res[i][1] = imm.getFiliacio();
				res[i][2] = imm.getDomicili();  
				res[i][3] = imm.getPoblacio();     
				res[i][4] = getNacio(imm.getNacio().getCodiNacio());  
				i++;                  
			} 
		return res;
		}       
	}
	
	public void carregaImmigrant(String passaport) throws Exception{
		immigrant = conImmigrant.find(passaport);
		if (immigrant == null) throw new Exception("L'immigrant "+passaport+" no esta donat d'alta al sistema.");
	}
	
	public String getFiliacio(){
		return immigrant.getFiliacio();
	}
	
	public String getDomicili(){
		return immigrant.getDomicili();
	}
	
	public String getPoblacio(){
		return immigrant.getPoblacio();
	}
	
	public String getCodinacio(){
		return immigrant.getNacio().getCodiNacio();
	}
	public String getPassaport(){
		return immigrant.getPassaport();
	}
	
	public String getHistorialImm() throws Exception {
		return conImmigrant.getHistorialImmigrant(this.getPassaport()); 
	}

	public String getNacio(String codi) throws Exception {
		if (codi.trim().isEmpty()) throw new Exception("El camp Nacio no pot estar buit.");
		ControladorGestioImmigrant controladorImmigrant = new ControladorGestioImmigrant();
		HashMap<String,String> nacions = new HashMap<String,String>();
		nacions = controladorImmigrant.getNacions();
		
		return nacions.get(codi);
	}
}
