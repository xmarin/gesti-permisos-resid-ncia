package Aplicacio;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;

import Aplicacio.Representations.AmfitrioRepresentation;
import Aplicacio.Representations.ImmigrantRepresentation;
import Aplicacio.Representations.PermisRepresentation;
import Aplicacio.Representations.RenovacioRepresentation;
import Domini.Amfitrio;
import Domini.Immigrant;
import Domini.PermisResidencia;
import Domini.RenovacioPermisResidencia;
import Persistencia.BBDDParentiu;
import Persistencia.BBDDPermis;
import Persistencia.BBDDRenovacio;

public class ControladorGestioPermis {
	
	private BBDDPermis connexioPermis;
	private BBDDRenovacio connexioRenovacio;
	private BBDDParentiu connexioParentiu;
	private ControladorGestioAmfitrio conAmf;
	private ControladorGestioImmigrant conImm;
	private HashSet<String> parentius;
	private Immigrant imm;
	private Amfitrio amf;
	private PermisResidencia permis;
	private RenovacioPermisResidencia renovacio;
	
	public ControladorGestioPermis() throws Exception{
		connexioParentiu = new BBDDParentiu();
		parentius = getParentius();
		connexioPermis = new BBDDPermis();
		connexioRenovacio = new BBDDRenovacio();
		conAmf = new ControladorGestioAmfitrio();
		conImm = new ControladorGestioImmigrant();
	}
	
	public String getDataActual(){
		Calendar dataActual = new GregorianCalendar();
		String data = (Integer.toString(dataActual.get(Calendar.DATE)) + "/" + Integer.toString(dataActual.get(Calendar.MONTH)+1)
				+ "/" + Integer.toString(dataActual.get(Calendar.YEAR)));
		return data;
	}
	
	public HashSet<String> getParentius() throws Exception{
		parentius = connexioParentiu.findParentius();
		return parentius;
	}

	public void carregaAmfitrio(String nif) throws Exception{
		amf = conAmf.existeixAmfitrio(nif);
	}

	public void carregaImmigrant(String pass) throws Exception{
		imm = conImm.carregaImmigrant(pass);
	}
	
	public void carregaPermis(String numPermis) throws Exception{
		if (numPermis.trim().isEmpty()) throw new Exception("El n�mero de perm�s no pot estar buit.");
		permis = connexioPermis.obtenirUltimPermis(numPermis);
		if (permis == null) throw new Exception("El perm�s especificat no existeix.");
	}
	
	public void carregaRenovacio(String numPermis) throws Exception{
		if (numPermis.trim().isEmpty()) throw new Exception("El n�mero de perm�s no pot estar buit.");
		renovacio = connexioRenovacio.obtenirUltimaRenovacio(numPermis);
		if (renovacio == null) throw new Exception("La renovaci� especificada no existeix.");
	}
	
	public ImmigrantRepresentation getImmigrantRepresentation() throws Exception{
		return new ImmigrantRepresentation(imm);
	}
	
	public AmfitrioRepresentation getAmfitrioRepresentation() throws Exception{
		return new AmfitrioRepresentation(amf);
	}
	
	public PermisRepresentation getPermisRepresentation() throws Exception{
		return new PermisRepresentation(permis); 
	}
	
	public RenovacioRepresentation getRenovacioRepresentation(String numPermis) throws Exception{
		if (numPermis.trim().isEmpty()) throw new Exception("El n�mero de perm�s no pot estar buit."); //para que no pete el parseint
		if (renovacio == null) throw new Exception("El perm�s especificat no existeix.");
		return new RenovacioRepresentation(renovacio); 
	}
	
	public String permisVigent() throws Exception{
		return permis.permisVigent();
	}
	
	public String renovacioVigent() throws Exception{
		carregaRenovacio(Integer.toString(permis.getNumPermis())); //ex si no hi ha cap renovacio
		return renovacio.renovacioVigent();
	}
	
	public String concessioPermis(String domicili, String durada, String parentiu) throws Exception{
		if (durada.trim().isEmpty()) throw new Exception("La durada no pot estar buida.");
		int numPermis = connexioPermis.obtenirNumPermis();
		GregorianCalendar a = new GregorianCalendar();
		a.getInstance();
		Date d = new Date(a.getTimeInMillis());
		PermisResidencia permis = new PermisResidencia(numPermis,amf, imm, d, durada, domicili, parentiu);
		connexioPermis.inserirPermis(permis);
		return Integer.toString(numPermis);
	}
	
	public void renovacioPermis(String domicili, String durada) throws Exception{
		GregorianCalendar a = permis.getDataCaducitat();
		a.add(a.DAY_OF_MONTH, 1);
		Date d = new Date(a.getTimeInMillis());
		RenovacioPermisResidencia ren = new RenovacioPermisResidencia(permis.getNumPermis(), d, durada, domicili);
		connexioRenovacio.inserirRenovacio(ren);
	}
	
	public void renovacioRenovacio(String domicili, String durada) throws Exception{
		GregorianCalendar a = renovacio.getDataCaducitat();
		a.add(a.DAY_OF_MONTH, 1);
		Date d = new Date(a.getTimeInMillis());
		RenovacioPermisResidencia ren = new RenovacioPermisResidencia(permis.getNumPermis(), d, durada, domicili);
		connexioRenovacio.inserirRenovacio(ren);
	}
	
	public String getDataCaducitatPermis(){
		GregorianCalendar c = new GregorianCalendar();
		c.setTimeInMillis(permis.getDataCaducitat().getTimeInMillis());
		c.add(c.DAY_OF_MONTH, 1);
		String res = Integer.toString(c.get(Calendar.DAY_OF_MONTH)) + "/" + Integer.toString(c.get(Calendar.MONTH)+1) + "/" + Integer.toString(c.get(Calendar.YEAR));
		return res;
		
	}
	
	public String getDataCaducitatRenovacio(){
		GregorianCalendar c = new GregorianCalendar();
		c.setTimeInMillis(renovacio.getDataCaducitat().getTimeInMillis());
		c.add(c.DAY_OF_MONTH, 1);
		String res = Integer.toString(c.get(Calendar.DAY_OF_MONTH)) + "/" + Integer.toString(c.get(Calendar.MONTH)+1) + "/" + Integer.toString(c.get(Calendar.YEAR));
		return res;
		
	}
}
