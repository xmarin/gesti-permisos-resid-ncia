package Aplicacio;

import java.sql.SQLException;

import Persistencia.BBDDFuncionari;
import Persistencia.ConnexioBBDD;

public class ControladorLogin {
	private ConnexioBBDD connexioBBDD;
	private BBDDFuncionari fun;
	
	public ControladorLogin()throws Exception{
		this.connexioBBDD = ConnexioBBDD.getConnexio();
	}

	public void login(String user, char[] pass) throws Exception{
		String pwd = new String(pass);
		if(user.trim().length()<=0 || pwd.trim().length()<=0) throw new Exception("Usuari o contrassenya incorrectes.");
		
		fun = new BBDDFuncionari(connexioBBDD);
		
		if (!fun.login(user, pwd))
			throw new Exception("Usuari o contrassenya incorrectes");
	}
	
	public void close() throws SQLException{
		connexioBBDD.close();
	}
}