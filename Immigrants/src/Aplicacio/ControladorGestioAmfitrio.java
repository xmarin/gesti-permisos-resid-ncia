package Aplicacio;

import Aplicacio.Representations.AmfitrioRepresentation;
import Domini.Amfitrio;
import Persistencia.BBDDAmfitrio;

public class ControladorGestioAmfitrio {
	private BBDDAmfitrio BDAmfitrio;
	private Amfitrio amfitrio;
	private String NIFamf;
	
	public ControladorGestioAmfitrio() throws Exception{
		BDAmfitrio = new BBDDAmfitrio();
	}
	
	public void afegirAmfitrio(String nif, String domicili, String filiacio, String poblacio)	throws Exception {
			if(BDAmfitrio.find(nif.toUpperCase())!=null){
				throw new Exception("L'amfitrio amb nif " + nif + " ja existeix.");
			}
			amfitrio = new Amfitrio(nif, domicili, filiacio, poblacio);
			BDAmfitrio.inserirAmfitrio(amfitrio);
	}
	
	public Amfitrio existeixAmfitrio(String nif) throws Exception {
		
		if (nif.trim().isEmpty()) throw new Exception("El NIF " +nif+ " �s incorrecte.");
		
		amfitrio = BDAmfitrio.find(nif.toUpperCase());
	
		if (amfitrio == null) throw new Exception("L'amfitri� " +nif+ " no esta donat d'alta al sistema.");
		
		return amfitrio;
	}
	
	public void guardarCanvis(String dom, String fil, String pob) throws Exception {
		
		amfitrio.setFiliacio(fil);
		amfitrio.setPoblacio(pob);
		amfitrio.setDomicili(dom);

		BDAmfitrio.modificarAmfitrio(amfitrio);
	}
	
	public AmfitrioRepresentation getAmfitrioRepresentation(String nif) throws Exception{
		existeixAmfitrio(nif);
		return new AmfitrioRepresentation(amfitrio);
	}
	
	public String getNIFamf(){
		return amfitrio.getNif();
	}
}