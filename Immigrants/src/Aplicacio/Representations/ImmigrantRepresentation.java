package Aplicacio.Representations;

import Domini.Immigrant;
import Domini.Nacio;

public class ImmigrantRepresentation {
	private String passaport, nom, domicili, poblacio;
	private Nacio nacio;
	
	public ImmigrantRepresentation(Immigrant i){
		this.passaport = i.getPassaport();
		this.nom = i.getFiliacio();
		this.domicili = i.getDomicili();
		this.poblacio = i.getPoblacio();
		this.nacio = i.getNacio();
	}
	
	public String getPassaport()	{ 	return passaport;}
	public String getFiliacio()	 	{	return nom;}
	public String getDomicili() 	{	return domicili;}
	public String getPoblacio()		{	return poblacio;}
	public String getCodiNacio() 	{	return nacio.getCodiNacio();}	
	public String getNacio()		{	return nacio.toString();}
	
	
}
