package Aplicacio.Representations;

import java.text.SimpleDateFormat;
import java.util.Date;

import Domini.Amfitrio;
import Domini.Immigrant;
import Domini.Parentiu;
import Domini.PermisResidencia;

public class PermisRepresentation {
	
	private int numPermis, duradaConcessio;
	private Amfitrio amfitrio;
	private Immigrant immigrant;
	private Date dataConcessio;
	private String domicili;
	private Parentiu parentiu;
	
	public PermisRepresentation(PermisResidencia p){
		numPermis = p.getNumPermis();
		duradaConcessio = p.getDuradaConcessio();
		amfitrio = p.getAmfitrio();
		immigrant = p.getImmigrant();
		dataConcessio = p.getDataConcessio();
		domicili = p.getDomicili();
		parentiu = p.getParentiu();
	}

	public String getNumPermis() 		{ return Integer.toString(numPermis);}
	public String getDuradaConcessio() 	{ return Integer.toString(duradaConcessio);}
	public String getNIFAmfitrio() 		{ return amfitrio.getNif();}
	public String getPassImmigrant() 	{ return immigrant.getPassaport();}
	public String getDomicili() 		{ return domicili;}
	public String getParentiu() 		{ return parentiu.toString();}
	public String getDataConcessio() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(dataConcessio);
	}
}
