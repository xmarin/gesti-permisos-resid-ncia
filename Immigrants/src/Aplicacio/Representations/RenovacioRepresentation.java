package Aplicacio.Representations;

import java.text.SimpleDateFormat;
import java.util.Date;

import Domini.Amfitrio;
import Domini.Immigrant;
import Domini.Parentiu;
import Domini.PermisResidencia;
import Domini.RenovacioPermisResidencia;

public class RenovacioRepresentation {

	private int numPermis, duradaRenovacio;
	private Date dataRenovacio;
	private String domicili;
	
	public RenovacioRepresentation(RenovacioPermisResidencia r){
		numPermis = r.getNumPermis();
		duradaRenovacio = r.getDuradaRenovacio();
		dataRenovacio = r.getDataRenovacio();
		domicili = r.getDomicili();
	}

	public String getNumPermis() 		{ return Integer.toString(numPermis);}
	public String getDuradaConcessio() 	{ return Integer.toString(duradaRenovacio);}
	public String getDomicili() 		{ return domicili;}
	public String getDataConcessio() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(dataRenovacio);
	}
	
}
