package Aplicacio.Representations;

import Domini.Amfitrio;

public class AmfitrioRepresentation {
	
	private String nif, domicili, filiacio, poblacio;
	
	public AmfitrioRepresentation(Amfitrio a){
		nif = a.getNif();
		domicili = a.getDomicili();
		filiacio = a.getFiliacio();
		poblacio = a.getPoblacio();
	}

	public String getNif() { return nif;}
	public String getDomicili() { return domicili;}
	public String getFiliacio() { return filiacio;}
	public String getPoblacio() { return poblacio;}
	
	

}
