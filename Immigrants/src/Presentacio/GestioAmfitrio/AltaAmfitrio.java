package Presentacio.GestioAmfitrio;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import Aplicacio.ControladorGestioAmfitrio;
import Presentacio.VistaPrincipal;
import Presentacio.GestioPermis.ConcessioPermis;

@SuppressWarnings("serial")
public class AltaAmfitrio extends JFrame {

	private JPanel contentPane;
	private JLabel lblPassaport;
	private JTextField txtNIF;
	private JLabel lblNom;
	private JTextField txtNom;
	private JLabel lblPrimerCognom;
	private JTextField txtDomicili;
	private JLabel lblSegonCognom;
	private JTextField txtPoblacio;
	private JButton btnGuardar;
	private ControladorGestioAmfitrio cont;
	private JFrame vp;
	private JButton btnReset;
	
	public AltaAmfitrio(VistaPrincipal vst){
		this.vp = vst;

		try {
			cont = new ControladorGestioAmfitrio();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error",
			        JOptionPane.ERROR_MESSAGE);
		}
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				tancarFinestra();
			}
		});
		initComponents();
	}
	
	public AltaAmfitrio(JFrame vst, String nif){
		this.vp = vst;

		try {
			cont = new ControladorGestioAmfitrio();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error",
			        JOptionPane.ERROR_MESSAGE);
		}
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				tancarFinestra();
			}
		});
		initComponents();
		txtNIF.setText(nif);
	}
	
	private void initComponents(){
		setTitle("Alta Amfitri�");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setBounds(100, 100, 340, 261);
		setLocationRelativeTo(null);
		setVisible(true);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(null);
		
		this.lblPassaport = new JLabel("NIF:");
		this.lblPassaport.setBounds(21, 17, 65, 16);
		this.contentPane.add(this.lblPassaport);
		
		this.txtNIF = new JTextField();
		this.txtNIF.setBounds(121, 11, 80, 28);
		this.contentPane.add(this.txtNIF);
		this.txtNIF.setColumns(10);
		
		this.lblNom = new JLabel("Filiaci�");
		this.lblNom.setBounds(21, 56, 65, 16);
		this.contentPane.add(this.lblNom);
		
		this.txtNom = new JTextField();
		this.txtNom.setBounds(121, 50, 183, 28);
		this.contentPane.add(this.txtNom);
		this.txtNom.setColumns(10);
		
		this.lblPrimerCognom = new JLabel("Domicili:");
		this.lblPrimerCognom.setBounds(21, 95, 102, 16);
		this.contentPane.add(this.lblPrimerCognom);
		
		this.txtDomicili = new JTextField();
		this.txtDomicili.setBounds(121, 89, 183, 28);
		this.contentPane.add(this.txtDomicili);
		this.txtDomicili.setColumns(10);
		
		this.lblSegonCognom = new JLabel("Poblaci�: ");
		this.lblSegonCognom.setBounds(21, 134, 102, 16);
		this.contentPane.add(this.lblSegonCognom);
		
		this.txtPoblacio = new JTextField();
		this.txtPoblacio.setBounds(121, 128, 183, 28);
		this.contentPane.add(this.txtPoblacio);
		this.txtPoblacio.setColumns(10);
		
		this.btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				afegirAmfitrio();
			}
		});
		this.btnGuardar.setBounds(121, 179, 96, 29);
		this.contentPane.add(this.btnGuardar);
		
		JButton btnTornar = new JButton("Tornar");
		btnTornar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				tancarFinestra();
			}
		});
		btnTornar.setBounds(227, 179, 77, 29);
		contentPane.add(btnTornar);
		
		btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnResetDades();
			}
		});
		btnReset.setBounds(22, 179, 89, 29);
		contentPane.add(btnReset);
	}

	protected void btnResetDades() {
		txtNIF.setText(null);
		txtNom.setText(null);
		txtDomicili.setText(null);
		txtPoblacio.setText(null);
	}

	protected void tancarFinestra() {
		vp.setEnabled(true);
		this.dispose();
	}

	protected void afegirAmfitrio() {
		try{
			cont.afegirAmfitrio(txtNIF.getText(), txtDomicili.getText(), txtNom.getText(), txtPoblacio.getText());
			JOptionPane.showMessageDialog(this,	
					"L'amfitri� s'ha afegit amb �xit.",
					"Alta Amfitri�",
					JOptionPane.INFORMATION_MESSAGE);
			if (vp instanceof ConcessioPermis) ((ConcessioPermis) vp).setNIF(txtNIF.getText());
			tancarFinestra();
		}catch(Exception e1){
			JOptionPane.showMessageDialog(this, 
					e1.getMessage(), 
					"Error alta amfitri�",
			        JOptionPane.ERROR_MESSAGE);
		}
		
	}
}