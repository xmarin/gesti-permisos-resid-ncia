package Presentacio.GestioAmfitrio;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import Aplicacio.ControladorGestioAmfitrio;
import Aplicacio.Representations.AmfitrioRepresentation;
import Presentacio.VistaPrincipal;


@SuppressWarnings("serial")
public class ModificacioAmfitrio extends JFrame {

	private JPanel contentPane;
	private JLabel lblNif;
	private JTextField txtNIF;
	private JLabel lblFiliacio;
	private JTextField txtFiliacio;
	private JLabel lblDomicili;
	private JTextField txtDomicili;
	private JLabel lblPoblacio;
	private JTextField txtPoblacio;
	private JButton btnGuardar, btnBuscar, btnReset, btnTornar;
	private JFrame vp;
	private	ControladorGestioAmfitrio mod;
	private String dni;

	public ModificacioAmfitrio(VistaPrincipal f) {
		this.vp = f;
		
		try {
			mod = new ControladorGestioAmfitrio();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error Gesti� Amfitri�",
			        JOptionPane.ERROR_MESSAGE);
		}
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				tancarFinestra();
			}
		});
		
		initComponents();
	}
	
	public ModificacioAmfitrio(JFrame f, String id) {
		this.vp = f;
		dni = id;
				
		try {
			mod = new ControladorGestioAmfitrio();
			mod.getAmfitrioRepresentation(dni);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error Gesti� Amfitri�",
			        JOptionPane.ERROR_MESSAGE);
		}
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				tancarFinestra();
			}
		});
		
		initComponents();
		
		btnBuscar.setEnabled(false);
		txtNIF.setEnabled(false);
		
		try {
			AmfitrioRepresentation a = mod.getAmfitrioRepresentation(dni);
			txtFiliacio.setText(a.getFiliacio());
			txtDomicili.setText(a.getDomicili());
			txtNIF.setText(a.getNif());
			txtPoblacio.setText(a.getPoblacio());
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(this, e1.getMessage(), "Error Gesti� Immigrant",
			        JOptionPane.ERROR_MESSAGE);
		}
		txtFiliacio.setEnabled(true);
		txtDomicili.setEnabled(true);
		txtPoblacio.setEnabled(true);
		btnTornar.setEnabled(true);
		btnGuardar.setEnabled(true);
	}
	
	
	public void initComponents(){
		setTitle("Modifica Amfitri�");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setBounds(100, 100, 365, 261);
		setLocationRelativeTo(null);
		setVisible(true);
		
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(null);
		
		this.lblNif = new JLabel("NIF:");
		this.lblNif.setBounds(21, 17, 65, 16);
		this.contentPane.add(this.lblNif);
		
		this.txtNIF = new JTextField();
		this.txtNIF.setBounds(121, 11, 97, 28);
		this.contentPane.add(this.txtNIF);
		this.txtNIF.setColumns(10);
		
		this.lblFiliacio = new JLabel("Filiaci�:");
		this.lblFiliacio.setBounds(21, 56, 65, 16);
		this.contentPane.add(this.lblFiliacio);
		
		this.txtFiliacio = new JTextField();
		txtFiliacio.setEnabled(false);
		this.txtFiliacio.setBounds(121, 50, 218, 28);
		this.contentPane.add(this.txtFiliacio);
		this.txtFiliacio.setColumns(10);
		
		this.lblDomicili = new JLabel("Domicili: ");
		this.lblDomicili.setBounds(21, 95, 102, 16);
		this.contentPane.add(this.lblDomicili);
		
		this.txtDomicili = new JTextField();
		txtDomicili.setEnabled(false);
		this.txtDomicili.setBounds(121, 89, 218, 28);
		this.contentPane.add(this.txtDomicili);
		this.txtDomicili.setColumns(10);
		
		this.lblPoblacio = new JLabel("Poblaci�: ");
		this.lblPoblacio.setBounds(21, 134, 102, 16);
		this.contentPane.add(this.lblPoblacio);
		
		this.txtPoblacio = new JTextField();
		txtPoblacio.setEnabled(false);
		this.txtPoblacio.setBounds(121, 128, 218, 28);
		this.contentPane.add(this.txtPoblacio);
		this.txtPoblacio.setColumns(10);
		
		this.btnGuardar = new JButton("Guardar");
		btnGuardar.setEnabled(false);
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					guardarCanvis();
			}
		});
		this.btnGuardar.setBounds(130, 179, 96, 29);
		this.contentPane.add(this.btnGuardar);
		
		btnTornar = new JButton("Tornar");
		btnTornar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				tancarFinestra();
			}
		});
		btnTornar.setBounds(242, 179, 96, 29);
		contentPane.add(btnTornar);
		
		btnBuscar = new JButton("Cercar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnBuscar(txtNIF.getText());
			}

		});
		btnBuscar.setBounds(228, 14, 96, 29);
		contentPane.add(btnBuscar);
		
		btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				resetDades();
			}
		});
		btnReset.setBounds(21, 180, 96, 29);
		contentPane.add(btnReset);
	}
	

	protected void resetDades() {
		btnBuscar.setEnabled(true);
		btnGuardar.setEnabled(false);
		txtNIF.setEnabled(true);
		txtNIF.setText(null);
		txtFiliacio.setText(null);
		txtFiliacio.setEnabled(false);
		txtDomicili.setText(null);
		txtDomicili.setEnabled(false);
		txtPoblacio.setText(null);
		txtPoblacio.setEnabled(false);		
	}
	
	protected void tancarFinestra() {
		vp.setEnabled(true);
		dispose();
	}
	
	private void btnBuscar(String nif) {
		
		try {
			AmfitrioRepresentation rep = mod.getAmfitrioRepresentation(nif);
			txtNIF.setEnabled(false);
			btnBuscar.setEnabled(false);
			
			txtFiliacio.setEnabled(true);
			txtDomicili.setEnabled(true);
			txtPoblacio.setEnabled(true);
			
			txtFiliacio.setText(rep.getFiliacio());
			txtDomicili.setText(rep.getDomicili());
			txtPoblacio.setText(rep.getPoblacio());
			
			btnGuardar.setEnabled(true);
			
		} catch (Exception e) {
		
			JOptionPane.showMessageDialog(this, e.getMessage(), "Amfitri� inexistent",
			        JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void guardarCanvis(){
		try {
			
			String fil = txtFiliacio.getText();
			String dom = txtDomicili.getText();
			String pob = txtPoblacio.getText();

			mod.guardarCanvis(dom,fil,pob);
			
			JOptionPane.showMessageDialog(this, "L'amfitri� amb NIF "+ txtNIF.getText() + " s'ha modificat amb �xit", "Modificaci� Amfitri�", JOptionPane.INFORMATION_MESSAGE);
			tancarFinestra();
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(this, e1.getMessage(), "L'amfitri� no s'ha pogut modificar.",
			        JOptionPane.ERROR_MESSAGE);
		}
	}
}
