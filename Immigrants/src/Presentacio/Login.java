package Presentacio;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.Font;

import Aplicacio.ControladorLogin;

@SuppressWarnings("serial")
public class Login extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldUser;
	private JPasswordField passwordField;
	private ControladorLogin contLogin;

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	public Login() {
		initComponents();
	}
	private void initComponents() {
		setVisible(true);
		try {
			contLogin = new ControladorLogin();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error",
			        JOptionPane.ERROR_MESSAGE);
		}
		
		setTitle("TCM Immigrants");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 347, 180);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		
		JLabel lblUsuari = new JLabel("Usuari");
		lblUsuari.setBounds(32, 48, 46, 14);
		contentPane.add(lblUsuari);
		
		JLabel lblContrassenya = new JLabel("Contrassenya");
		lblContrassenya.setBounds(32, 73, 83, 14);
		contentPane.add(lblContrassenya);
		
		JButton btnNewButton = new JButton("Accedir");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				login();
			}
		});
		btnNewButton.setBounds(59, 107, 89, 23);
		contentPane.add(btnNewButton);
		
		textFieldUser = new JTextField();
		textFieldUser.setBounds(161, 45, 126, 20);
		contentPane.add(textFieldUser);
		textFieldUser.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(161, 70, 126, 20);
		contentPane.add(passwordField);
		
		JButton btnSortir = new JButton("Sortir");
		btnSortir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tancarFinestra();
			}
		});
		btnSortir.setBounds(182, 107, 89, 23);
		contentPane.add(btnSortir);
		
		JLabel lblAccsATcm = new JLabel("Acces a TCM Immigrants");
		lblAccsATcm.setFont(new Font("Verdana", Font.ITALIC, 16));
		lblAccsATcm.setBounds(10, 11, 243, 26);
		contentPane.add(lblAccsATcm);
	}
	
	protected void tancarFinestra() {
		this.dispose();
	}

	private void login() {
		try {
			contLogin.login(textFieldUser.getText(), passwordField.getPassword());
			new VistaPrincipal(contLogin,textFieldUser.getText());
			dispose();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error Login",
			        JOptionPane.ERROR_MESSAGE);
		}
	}
}
