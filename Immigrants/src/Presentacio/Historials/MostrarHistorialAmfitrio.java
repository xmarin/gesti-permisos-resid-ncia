package Presentacio.Historials;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JLabel;

import Aplicacio.ControladorHistorialAmfitrio;
import Aplicacio.ControladorHistorialImmigrant;

import java.awt.Font;

import javax.swing.JSeparator;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


@SuppressWarnings("serial")
public class MostrarHistorialAmfitrio extends JFrame {
	private HistorialAmfitrio histA;
	private ControladorHistorialAmfitrio controladorHistorialAmf;
	private JPanel contentPane;
	private JTextField textPob;
	private JTextField textNIF;
	private JTextField textDom;
	private JScrollPane scrollPane;
	private DefaultTableModel dtm;
	private JTable table, tblHistorialAmf;
	private String[] columnNames = {"Passaport", "Filiaci�", "Nacionalitat", "Parentiu", "Data Invitaci�", "Data Retorn"};
	
	/**
	 * Create the frame.
	 */
	public MostrarHistorialAmfitrio(HistorialAmfitrio histA, String NIF) {
		this.histA = histA;
		try {
			controladorHistorialAmf = new ControladorHistorialAmfitrio();
			controladorHistorialAmf.carregaAmfitrio(NIF);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(new JFrame(), e.getMessage(), "Error",                           
					JOptionPane.ERROR_MESSAGE);             
		} 
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent arg0) {
				tancarFinestra();
			}
		});

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 645, 491);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblH = new JLabel("Historial Amifitrio de " + controladorHistorialAmf.getFiliacio());
		lblH.setFont(new Font("Verdana", Font.PLAIN, 16));
		lblH.setBounds(10, 11, 601, 28);
		contentPane.add(lblH);
		
		JLabel lblDomicili = new JLabel("Domicili");
		lblDomicili.setBounds(331, 53, 79, 14);
		contentPane.add(lblDomicili);
		
		JLabel lblPoblacio = new JLabel("Poblaci\u00F3");
		lblPoblacio.setBounds(331, 81, 79, 14);
		contentPane.add(lblPoblacio);
		
		JLabel lblNom = new JLabel("");
		lblNom.setBounds(140, 24, 46, 14);
		contentPane.add(lblNom);
		
		textPob = new JTextField();
		textPob.setEditable(false);
		textPob.setBounds(401, 78, 210, 20);
		contentPane.add(textPob);
		textPob.setColumns(10);
		textPob.setText(controladorHistorialAmf.getPoblacio());
		
		textNIF = new JTextField();
		textNIF.setEditable(false);
		textNIF.setBounds(61, 48, 86, 20);
		contentPane.add(textNIF);
		textNIF.setColumns(10);
		textNIF.setText(controladorHistorialAmf.getNif());
		
		textDom = new JTextField();
		textDom.setEditable(false);
		textDom.setBounds(401, 50, 210, 20);
		contentPane.add(textDom);
		textDom.setColumns(10);
		textDom.setText(controladorHistorialAmf.getDomicili());
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 34, 601, 23);
		contentPane.add(separator);
		
		JLabel lblNewLabel = new JLabel("NIF");
		lblNewLabel.setBounds(20, 51, 46, 14);
		contentPane.add(lblNewLabel);
		setLocationRelativeTo(null);
		setVisible(true);
		setTitle("Historial Amfitri�: " + NIF);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(20, 137, 591, 256);
		contentPane.add(scrollPane);
		
		dtm = new DefaultTableModel(null,columnNames);
		table = new JTable(dtm);
		scrollPane.setViewportView(table);
	
		
		JButton btnTornar = new JButton("Tornar");
		btnTornar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				tancarFinestra();
			}
		});
		btnTornar.setBounds(522, 419, 89, 23);
		contentPane.add(btnTornar);
			
		updateTable();
		//tblHistorialAmf.getTableHeader().setReorderingAllowed(false);				

	}

	private void tancarFinestra() {
		histA.setEnabled(true);
		dispose();
	}
	
	private void updateTable(){  
		try { 
			tblHistorialAmf = new JTable(controladorHistorialAmf.getHistorialAmf(), columnNames){ 
				public boolean isCellEditable(int row, int column) { 
					return false;                               
				}   
			}; 
		} catch (Exception e1) {   
			JOptionPane.showMessageDialog(this, "No s'ha pogut carregar l'historial.", "Historial Amfitri�", 
					JOptionPane.ERROR_MESSAGE);
		} 
		
		scrollPane.setViewportView(tblHistorialAmf);    
	}
}
