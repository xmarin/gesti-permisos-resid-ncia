package Presentacio.Historials;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import Aplicacio.ControladorGestioImmigrant;
import Aplicacio.ControladorHistorialImmigrant;
import Presentacio.VistaPrincipal;

import java.awt.Font;

import javax.swing.JSeparator;
import javax.swing.JScrollPane;
import javax.swing.JTable;

@SuppressWarnings("serial")
public class HistorialImmigrant extends JFrame {

	private JPanel contentPane;
	private JTextField txtSearch;
	private ControladorHistorialImmigrant controladorHistorialImm;
	private JFrame vistaPrincipal;
	private JButton btnReset;
	private JTable tblimmigrants, table;
	private JScrollPane scrollPane;
	private String[] columnNames = {"Num passaport", "Filiaci�", "Domicili", "Poblaci�", "Codi naci�"};
	private DefaultTableModel dtm;
	/**
	 * 
	 * Create the frame.
	 * @throws Exception 
	 */
	
	public HistorialImmigrant(VistaPrincipal vst){
		this.vistaPrincipal = vst;

		try {
			controladorHistorialImm = new ControladorHistorialImmigrant();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error",
			        JOptionPane.ERROR_MESSAGE);
		}
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				tancarFinestra();
			}
		});
		initComponents();
	}
	
	private void initComponents(){
		setTitle("Historial Immigrant");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setBounds(100, 100, 605, 415);
		setLocationRelativeTo(null);
		setVisible(true);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		contentPane.setLayout(null);
		
		this.txtSearch = new JTextField();
		txtSearch.setBounds(46, 70, 111, 28);
		this.contentPane.add(this.txtSearch);
		this.txtSearch.setColumns(10);
		
		JButton btnTornar = new JButton("Tornar");
		btnTornar.setBounds(502, 337, 77, 29);
		btnTornar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				tancarFinestra();
			}
		});
		contentPane.add(btnTornar);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(21, 109, 558, 217);
		contentPane.add(scrollPane);
		
		dtm = new DefaultTableModel(null,columnNames);
		table = new JTable(dtm);
		scrollPane.setViewportView(table);    
		
		btnReset = new JButton("Cercar");
		btnReset.setBounds(167, 73, 94, 23);
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateTable();
			}
		});
		contentPane.add(btnReset);
		
		JLabel lblNewLabel = new JLabel("Historial Immigrant: ");
		lblNewLabel.setBounds(21, 11, 558, 23);
		lblNewLabel.setFont(new Font("Verdana", Font.PLAIN, 16));
		contentPane.add(lblNewLabel);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(21, 36, 558, 23);
		contentPane.add(separator);
		
		JLabel lblNewLabel_1 = new JLabel("Introdueix Filiaci� o Passaport del immigrant");
		lblNewLabel_1.setBounds(21, 45, 366, 14);
		contentPane.add(lblNewLabel_1);	
		
		table.getTableHeader().setReorderingAllowed(false);		
		tblimmigrants = new JTable();
	}

	protected void tancarFinestra() {
		vistaPrincipal.setEnabled(true);
		this.dispose();
	}
	
	private void updateTable(){  
		try { 
			if (txtSearch.getText().trim().length() != 0){
				tblimmigrants = new JTable(controladorHistorialImm.searchImmigrant(txtSearch.getText()), columnNames){ 
					public boolean isCellEditable(int row, int column) { 
						return false;                               
					}                   
				}; 
				
				tblimmigrants.getTableHeader().setReorderingAllowed(false);
				tblimmigrants.setVisible(true);
				
				tblimmigrants.addMouseListener(new MouseAdapter() { 
					public void mousePressed(MouseEvent me) {
						JTable table =(JTable) me.getSource(); 
						if (me.getClickCount() == 2) { 
							MostrarHistorial(table.getValueAt(table.getSelectedRow(), 0).toString()); 
						}                       
					}              
				});  

				scrollPane.setViewportView(tblimmigrants);     
			}else{
				tblimmigrants.setVisible(false);
				scrollPane.setViewportView(table);
			}       
		} catch (Exception e1) { 
			JOptionPane.showMessageDialog(this, "No s'ha trobat cap immigrant amb aquests par�metres", "Historial Immigrant", 
					JOptionPane.ERROR_MESSAGE);           
		} 
	}
	
	private void MostrarHistorial(String passaportImmigrant) {
		setEnabled(false);
		new MostrarHistorialImmigrant(this,passaportImmigrant);
	}  
}