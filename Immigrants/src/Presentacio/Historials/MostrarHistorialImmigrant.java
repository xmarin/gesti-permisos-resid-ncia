package Presentacio.Historials;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JLabel;

import Aplicacio.ControladorHistorialImmigrant;

import java.awt.Font;

import javax.swing.JSeparator;
import javax.swing.JTextArea;

public class MostrarHistorialImmigrant extends JFrame {
	private HistorialImmigrant histI;
	private ControladorHistorialImmigrant controladorHistorialImm;
	private JPanel contentPane;
	private JTextField textPob;
	private JTextField textPassaport;
	private JTextField textDom;
	private DefaultTableModel dtm;
	private String[] columnNames = {"Permis", "Amfitrio", "Parentiu", "Data Concessio", "Data Retorn", "Domicili"};
	private JTextField txtNacio;
	
	/**
	 * Create the frame.
	 */
	public MostrarHistorialImmigrant(HistorialImmigrant histI, String passaport) {
		this.histI = histI;
		try {
			controladorHistorialImm = new ControladorHistorialImmigrant();
			controladorHistorialImm.carregaImmigrant(passaport);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(new JFrame(), e.getMessage(), "Error",                           
					JOptionPane.ERROR_MESSAGE);             
		} 
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent arg0) {
				tancarFinestra();
			}
		});

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 637, 494);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblH = new JLabel("Historial Immigrant de " + controladorHistorialImm.getFiliacio());
		lblH.setFont(new Font("Verdana", Font.PLAIN, 16));
		lblH.setBounds(10, 11, 601, 28);
		contentPane.add(lblH);
		
		JLabel lblDomicili = new JLabel("Domicili");
		lblDomicili.setBounds(317, 51, 79, 14);
		contentPane.add(lblDomicili);
		
		JLabel lblPoblacio = new JLabel("Poblaci\u00F3");
		lblPoblacio.setBounds(317, 79, 79, 14);
		contentPane.add(lblPoblacio);
		
		textPob = new JTextField();
		textPob.setEditable(false);
		textPob.setBounds(401, 76, 210, 20);
		contentPane.add(textPob);
		textPob.setColumns(10);
		textPob.setText(controladorHistorialImm.getPoblacio());
		
		textPassaport = new JTextField();
		textPassaport.setEditable(false);
		textPassaport.setBounds(100, 50, 99, 20);
		contentPane.add(textPassaport);
		textPassaport.setColumns(10);
		textPassaport.setText(controladorHistorialImm.getPassaport());
		
		textDom = new JTextField();
		textDom.setEditable(false);
		textDom.setBounds(401, 50, 210, 20);
		contentPane.add(textDom);
		textDom.setColumns(10);
		textDom.setText(controladorHistorialImm.getDomicili());
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 34, 601, 23);
		contentPane.add(separator);
		
		JLabel lblNewLabel = new JLabel("Passaport");
		lblNewLabel.setBounds(20, 51, 79, 14);
		contentPane.add(lblNewLabel);
		setLocationRelativeTo(null);
		setVisible(true);
		setTitle("Historial Immigrant: " + passaport);
		
		dtm = new DefaultTableModel(null,columnNames);
	
		
		JButton btnTornar = new JButton("Tornar");
		btnTornar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				tancarFinestra();
			}
		});
		btnTornar.setBounds(522, 419, 89, 23);
		contentPane.add(btnTornar);
		
		JLabel lblNacio = new JLabel("Naci\u00F3");
		lblNacio.setBounds(317, 107, 79, 14);
		contentPane.add(lblNacio);
		
		txtNacio = new JTextField();

		
		try {
			txtNacio.setText(controladorHistorialImm.getNacio(controladorHistorialImm.getCodinacio()));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(new JFrame(), e.getMessage(), "Error",                           
					JOptionPane.ERROR_MESSAGE);   
		}
		txtNacio.setEditable(false);
		txtNacio.setColumns(10);
		txtNacio.setBounds(401, 104, 210, 20);
		contentPane.add(txtNacio);
		
		JTextArea textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setBounds(10, 145, 601, 152);
		try {
			textArea.setText(controladorHistorialImm.getHistorialImm());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(20, 137, 591, 256);
		contentPane.add(scrollPane);
		
		scrollPane.setViewportView(textArea);

	}

	private void tancarFinestra() {
		histI.setEnabled(true);
		dispose();
	}
}
