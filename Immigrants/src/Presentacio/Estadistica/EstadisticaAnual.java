package Presentacio.Estadistica;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import Aplicacio.ControladorEstadisticaAnual;
import Presentacio.VistaPrincipal;

import java.awt.Font;

import javax.swing.JSeparator;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class EstadisticaAnual extends JFrame {
	private JPanel contentPane;
	private JTextField txtSearch;
	private ControladorEstadisticaAnual contEA;
	private JFrame vistaPrincipal;
	private JButton btnReset;
	private JLabel lblNewLabel;
	private JScrollPane scrollPane;
	private JTextArea textArea;
	/**
	 * 
	 * Create the frame.
	 * @throws Exception 
	 */
	
	public EstadisticaAnual(VistaPrincipal vst){
		this.vistaPrincipal = vst;

		try {
			contEA = new ControladorEstadisticaAnual();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error",
			        JOptionPane.ERROR_MESSAGE);
		}
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				tancarFinestra();
			}
		});
		initComponents();
	}
	
	private void initComponents(){
		setTitle("Estad\u00EDstica Anual");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setBounds(100, 100, 605, 415);
		setLocationRelativeTo(null);
		setVisible(true);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		contentPane.setLayout(null);
		
		this.txtSearch = new JTextField();
		txtSearch.setBounds(21, 70, 111, 28);
		this.contentPane.add(this.txtSearch);
		this.txtSearch.setColumns(10);
		
		JButton btnTornar = new JButton("Tornar");
		btnTornar.setBounds(502, 337, 77, 29);
		btnTornar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				tancarFinestra();
			}
		});
		contentPane.add(btnTornar);
		
		btnReset = new JButton("Generar");
		btnReset.setBounds(142, 73, 94, 23);
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				generarEstadistica();
			}
		});
		contentPane.add(btnReset);
		
		lblNewLabel = new JLabel("Estad\u00EDstica Anual:");
		lblNewLabel.setBounds(21, 11, 558, 23);
		lblNewLabel.setFont(new Font("Verdana", Font.PLAIN, 16));
		contentPane.add(lblNewLabel);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(21, 36, 558, 23);
		contentPane.add(separator);
		
		JLabel lblNewLabel_1 = new JLabel("Introdueix any estadística");
		lblNewLabel_1.setBounds(21, 45, 366, 26);
		contentPane.add(lblNewLabel_1);	
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(21, 109, 558, 217);
		contentPane.add(scrollPane);
		
		textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
	}

	protected void tancarFinestra() {
		vistaPrincipal.setEnabled(true);
		this.dispose();
	}
	
	private void generarEstadistica() {
		try {
			contEA.getEstadisticaAnual(txtSearch.getText());
			lblNewLabel.setText("Estadística Anual: " + txtSearch.getText());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error",
			        JOptionPane.ERROR_MESSAGE);
		}
		
	}
}