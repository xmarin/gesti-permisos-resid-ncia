package Presentacio.GestioPermis;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import Aplicacio.ControladorGestioPermis;
import Aplicacio.Representations.AmfitrioRepresentation;
import Aplicacio.Representations.ImmigrantRepresentation;
import Aplicacio.Representations.PermisRepresentation;
import Presentacio.GestioAmfitrio.ModificacioAmfitrio;
import Presentacio.GestioImmigrant.ModificacioImmigrant;


@SuppressWarnings("serial")
public class RenovacioPermis extends JFrame {
	private JFrame vistaPrincipal;
	private JPanel contentPane;
	private JTextField textFieldPassaport;
	private JTextField textFieldPassImm;
	private JTextField textFieldFiliacioImm;
	private JTextField textFieldNacio;
	private JTextField textFieldDurada;
	private JTextField textFieldDomImm;
	private JTextField textFieldRenovacio;
	private JTextField textFieldNIF;
	private JTextField textFieldFiliacioAmf;
	private JTextField textFieldPobAmf;
	private JButton btnRenovar;
	private JButton btnModifica, btnModifica_1, btnActualitza, btnActualitza_1;
	ImmigrantRepresentation immi;
	AmfitrioRepresentation amf;
	boolean renovacio;
	
	private ControladorGestioPermis controladorPermis;
	private JTextField textFieldDomAmf;
	
	public RenovacioPermis(JFrame vst) {
		
		this.vistaPrincipal = vst;
		renovacio = false;
		try {
			controladorPermis =  new ControladorGestioPermis();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error controlador",JOptionPane.ERROR_MESSAGE);
		}
		initComponents();
	}
	
	public RenovacioPermis(JFrame vst, String passaport) {
		
		this.vistaPrincipal = vst;
		renovacio = false;
		try {
			controladorPermis =  new ControladorGestioPermis();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error controlador",JOptionPane.ERROR_MESSAGE);
		}
		initComponents();
		textFieldPassaport.setText(passaport);
		btnVerificar();
	}
	
	//PRIVATE METHODS
	private void initComponents(){
		setResizable(false);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent arg0) {
				tancarFinestra();
			}
		});
		
		setTitle("Renovaci� de perm�s");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 726, 510);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.setVisible(true);
		setLocationRelativeTo(null);
		contentPane.setLayout(null);
		
		JLabel lblRenovaciDePerms = new JLabel("Renovaci� de perm�s de resid�ncia");
		lblRenovaciDePerms.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 13));
		lblRenovaciDePerms.setBounds(34, 29, 229, 16);
		contentPane.add(lblRenovaciDePerms);
		
		JLabel labelPassaport = new JLabel("Passaport de l'immigrant:");
		labelPassaport.setBounds(34, 72, 179, 16);
		contentPane.add(labelPassaport);
		
		textFieldPassaport = new JTextField();
		textFieldPassaport.setColumns(10);
		textFieldPassaport.setBounds(204, 67, 148, 28);
		contentPane.add(textFieldPassaport);
		
		JButton buttonVerificar = new JButton("Verificar");
		buttonVerificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnVerificar();
			}
		});
		buttonVerificar.setBounds(362, 66, 94, 29);
		contentPane.add(buttonVerificar);
		
		JLabel lblDataRenovacio = new JLabel("Data renovaci�: ");
		lblDataRenovacio.setBounds(456, 398, 109, 14);
		contentPane.add(lblDataRenovacio);
		
		textFieldDurada = new JTextField();
		textFieldDurada.setEditable(false);
		textFieldDurada.setEnabled(false);
		textFieldDurada.setBounds(340, 395, 89, 20);
		contentPane.add(textFieldDurada);
		textFieldDurada.setColumns(10);
		
		JLabel lblDurada = new JLabel("Durada: ");
		lblDurada.setBounds(292, 398, 73, 14);
		contentPane.add(lblDurada);
		
		textFieldDomImm = new JTextField();
		textFieldDomImm.setEnabled(false);
		textFieldDomImm.setEditable(false);
		textFieldDomImm.setBounds(112, 395, 162, 20);
		contentPane.add(textFieldDomImm);
		textFieldDomImm.setColumns(10);
		
		JLabel lblDomicili = new JLabel("Domicili: ");
		lblDomicili.setBounds(39, 398, 63, 14);
		contentPane.add(lblDomicili);
		
		textFieldRenovacio = new JTextField();
		textFieldRenovacio.setBounds(553, 395, 98, 20);
		contentPane.add(textFieldRenovacio);
		textFieldRenovacio.setColumns(10);
		textFieldRenovacio.setEditable(false);
		
		btnRenovar = new JButton("Renovar");
		btnRenovar.setEnabled(false);
		btnRenovar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnRenovar();
			}
		});
		btnRenovar.setBounds(307, 447, 89, 23);
		contentPane.add(btnRenovar);
		
		JButton btnTornar = new JButton("Tornar");
		btnTornar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				tancarFinestra();
			}
		});
		btnTornar.setBounds(431, 447, 89, 23);
		contentPane.add(btnTornar);
		
		JLabel lblDades = new JLabel("Dades de la renovaci� del perm�s:");
		lblDades.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 11));
		lblDades.setBounds(34, 368, 342, 16);
		contentPane.add(lblDades);
		
		JPanel panelImm = new JPanel();
		panelImm.setBounds(34, 106, 661, 114);
		contentPane.add(panelImm);
		TitledBorder titleImmi;
		titleImmi = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black),"Informaci� de l'immigrant");
		panelImm.setBorder(titleImmi);
		panelImm.setLayout(null);
		
		textFieldPassImm = new JTextField();
		textFieldPassImm.setBounds(79, 39, 126, 20);
		panelImm.add(textFieldPassImm);
		textFieldPassImm.setEditable(false);
		textFieldPassImm.setToolTipText("");
		textFieldPassImm.setColumns(10);
		
		JLabel lblPassaportImm = new JLabel("Passaport: ");
		lblPassaportImm.setBounds(12, 42, 91, 14);
		panelImm.add(lblPassaportImm);
		
		JLabel lblFiliacioImm = new JLabel("Filiaci�: ");
		lblFiliacioImm.setBounds(226, 42, 59, 14);
		panelImm.add(lblFiliacioImm);
		
		textFieldFiliacioImm = new JTextField();
		textFieldFiliacioImm.setBounds(280, 39, 161, 20);
		panelImm.add(textFieldFiliacioImm);
		textFieldFiliacioImm.setEditable(false);
		textFieldFiliacioImm.setColumns(10);
		
		JLabel lblNacio = new JLabel("Naci�: ");
		lblNacio.setBounds(457, 42, 46, 14);
		panelImm.add(lblNacio);
		
		textFieldNacio = new JTextField();
		textFieldNacio.setBounds(500, 39, 114, 20);
		panelImm.add(textFieldNacio);
		textFieldNacio.setEditable(false);
		textFieldNacio.setColumns(10);
		
		btnActualitza = new JButton("Actualitza");
		btnActualitza.setEnabled(false);
		btnActualitza.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnActualitzaImmi();
			}
		});
		btnActualitza.setBounds(500, 80, 114, 23);
		panelImm.add(btnActualitza);
		
		btnModifica = new JButton("Modificar");
		btnModifica.setEnabled(false);
		btnModifica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnModificarImmi();
			}
		});
		btnModifica.setBounds(371, 80, 114, 23);
		panelImm.add(btnModifica);
		
		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnReset();
			}
		});
		btnReset.setBounds(185, 447, 89, 23);
		contentPane.add(btnReset);
		
		JPanel panelAmfitrio = new JPanel();
		panelAmfitrio.setLayout(null);
		panelAmfitrio.setBounds(34, 231, 661, 126);
		this.contentPane.add(panelAmfitrio);
		TitledBorder titleAmf;
		titleAmf = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black),"Informaci� de l'amfitri�");
		panelAmfitrio.setBorder(titleAmf);
		panelAmfitrio.setLayout(null);		
		
		this.textFieldNIF = new JTextField();
		this.textFieldNIF.setToolTipText("");
		this.textFieldNIF.setEditable(false);
		this.textFieldNIF.setColumns(10);
		this.textFieldNIF.setBounds(76, 39, 99, 20);
		panelAmfitrio.add(this.textFieldNIF);
		
		JLabel labelNIF = new JLabel("NIF:");
		labelNIF.setBounds(12, 42, 91, 14);
		panelAmfitrio.add(labelNIF);
		
		JLabel labelFiliacioAmf = new JLabel("Filiaci�: ");
		labelFiliacioAmf.setBounds(198, 42, 59, 14);
		panelAmfitrio.add(labelFiliacioAmf);
		
		this.textFieldFiliacioAmf = new JTextField();
		this.textFieldFiliacioAmf.setEditable(false);
		this.textFieldFiliacioAmf.setColumns(10);
		this.textFieldFiliacioAmf.setBounds(253, 38, 161, 20);
		panelAmfitrio.add(this.textFieldFiliacioAmf);
		
		JLabel labelPoblacio = new JLabel("Poblaci�:");
		labelPoblacio.setBounds(12, 86, 62, 14);
		panelAmfitrio.add(labelPoblacio);
		
		this.textFieldPobAmf = new JTextField();
		this.textFieldPobAmf.setEditable(false);
		this.textFieldPobAmf.setColumns(10);
		this.textFieldPobAmf.setBounds(76, 83, 114, 20);
		panelAmfitrio.add(this.textFieldPobAmf);
		
		btnModifica_1 = new JButton("Modificar");
		btnModifica_1.setEnabled(false);
		btnModifica_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnModificarAmf();
			}
		});
		btnModifica_1.setBounds(376, 92, 114, 23);
		panelAmfitrio.add(btnModifica_1);
		
		btnActualitza_1 = new JButton("Actualitza");
		btnActualitza_1.setEnabled(false);
		btnActualitza_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnActualitzaAmf();
			}
		});
		btnActualitza_1.setBounds(500, 92, 114, 23);
		panelAmfitrio.add(btnActualitza_1);
		
		JLabel lblDomiciliAmf = new JLabel("Domicili: ");
		lblDomiciliAmf.setBounds(441, 42, 77, 14);
		panelAmfitrio.add(lblDomiciliAmf);
		
		textFieldDomAmf = new JTextField();
		textFieldDomAmf.setEditable(false);
		textFieldDomAmf.setBounds(500, 39, 114, 20);
		panelAmfitrio.add(textFieldDomAmf);
		textFieldDomAmf.setColumns(10);
	}
	
	protected void btnModificarAmf() {
		setEnabled(false);
		new ModificacioAmfitrio(this, textFieldNIF.getText());
	}

	protected void btnModificarImmi() {
		setEnabled(false);
		new ModificacioImmigrant(this, textFieldPassaport.getText());
	}

	protected void btnActualitzaAmf() {
		try {
			controladorPermis.carregaAmfitrio(textFieldNIF.getText());
			amf = controladorPermis.getAmfitrioRepresentation();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error al buscar l'immigrant",JOptionPane.ERROR_MESSAGE);
		}
		mostrarInformacio();
	}

	protected void btnActualitzaImmi() {
		try {
			controladorPermis.carregaImmigrant(textFieldPassaport.getText()); //busca immigrant a la bd, ex si no hi es
			immi = controladorPermis.getImmigrantRepresentation();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error al buscar l'immigrant",JOptionPane.ERROR_MESSAGE);
		}
		mostrarInformacio();
	}

	protected void btnVerificar() {
		try {
			controladorPermis.carregaImmigrant(textFieldPassaport.getText()); //carrega l'immigrant, Ex si no existeix
			
			try {
				controladorPermis.carregaPermis(textFieldPassaport.getText()); 	//busca a la BBDD i el carrega, si no en te Ex
				String accio = controladorPermis.permisVigent(); //mira si t� un permis vigent i retorna un String amb l'accio a realitzar
				btnRenovar.setEnabled(true);
				if (accio.equals("renovar")){ //es pot renovar el permis
					try{
						String accioRenovacio = controladorPermis.renovacioVigent(); //comprovar renovacions abans
						if (accioRenovacio.equals("no")){ //te una renovacio vigent, no pot renovar
							JOptionPane.showMessageDialog(this, "L'immigrant ja te una renovaci� vigent.", "Error Renovaci� Perm�s",JOptionPane.ERROR_MESSAGE);
						}
					} catch (Exception e2){ //no te renovacions vigents, renovar permis
						campsImmigrantRepresentation();
						textFieldRenovacio.setText(controladorPermis.getDataCaducitatPermis());
					}
				} else if (accio.equals("comprobarRenovacions")){ //el permis no es pot renovar, cal comprovar si la ultima renovacio si
					try{
						//carrega la ultima renovacio i retorna string amb accio a realitzar, ex si no hi ha cap
						String accioRenovar = controladorPermis.renovacioVigent(); 
						
						if (accioRenovar.equals("renovar")){ //concedir renovacio
							campsImmigrantRepresentation();
							textFieldRenovacio.setText(controladorPermis.getDataCaducitatRenovacio());
							renovacio = true;
						} else if (accioRenovar.equals("no")){ //te una renovacio vigent, no pot renovar
							JOptionPane.showMessageDialog(this, "L'immigrant ja te una renovaci� vigent.", "Error Renovaci� Perm�s",JOptionPane.ERROR_MESSAGE);
						} else if (accioRenovar.equals("concedir")){ //ni permis ni renovacio renovable, a concedir permis
							JOptionPane.showMessageDialog(this, "L'immigrant no pot renovar, cal concedir un perm�s nou", "Error Renovaci� Perm�s",JOptionPane.ERROR_MESSAGE);
							this.setEnabled(false);
							new ConcessioPermis(this.vistaPrincipal, textFieldPassaport.getText());
						}
					} catch (Exception e1){ //no hi ha cap renovacio i el permis no esta vigent, per tant concedir
						JOptionPane.showMessageDialog(this, "L'immigrant no pot renovar perm�s, cal concedir un nou", "Error Renovaci� Perm�s",JOptionPane.ERROR_MESSAGE);
						this.dispose();
						new ConcessioPermis(this.vistaPrincipal, textFieldPassaport.getText());
					}
				} else if (accio.equals("no")){ //t� un permis vigent, no pot renovar
					JOptionPane.showMessageDialog(this, "L'immigrant ja t� un permis vigent.", "Error Renovaci� Perm�s",JOptionPane.ERROR_MESSAGE);
				}
			} catch (Exception e){ //no te cap permis, no pot renovar
				JOptionPane.showMessageDialog(this, e.getMessage(), "Error Renovaci� Perm�s",JOptionPane.ERROR_MESSAGE);
				//conessioPermis
			}	

		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error Renovaci� Perm�s",JOptionPane.ERROR_MESSAGE);
			this.vistaPrincipal.setEnabled(true);
			this.dispose();
		}
	}
	
	protected void btnRenovar() {
		try {
			if (renovacio) controladorPermis.renovacioRenovacio(textFieldDomImm.getText(),textFieldDurada.getText());
			else controladorPermis.renovacioPermis(textFieldDomImm.getText(),textFieldDurada.getText());
			JOptionPane.showMessageDialog(this,"L'immigrant ha renovat el perm�s amb �xit.","Renovaci� perm�s",JOptionPane.INFORMATION_MESSAGE);
			tancarFinestra();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error renovant perm�s",JOptionPane.ERROR_MESSAGE);
		}
		
	} 

	protected void btnReset() {
		textFieldPassImm.setText(null);
		textFieldNIF.setText(null);
		textFieldPassaport.setText(null);
		textFieldFiliacioImm.setText(null);
		textFieldDomImm.setText(null);
		textFieldFiliacioAmf.setText(null);
		textFieldPobAmf.setText(null);
		textFieldDurada.setText(null);
		textFieldNacio.setText(null);
		textFieldDomImm.setEnabled(false);
		textFieldDomImm.setEditable(false);
		textFieldDurada.setEnabled(false);
		textFieldDurada.setEditable(false);
		btnRenovar.setEnabled(false);
		btnActualitza.setEnabled(false);
		btnActualitza_1.setEnabled(false);
		btnModifica.setEnabled(false);
		btnModifica_1.setEnabled(false);
	}

	

	private void campsImmigrantRepresentation(){
		
		try {
			PermisRepresentation pr = controladorPermis.getPermisRepresentation();
			ImmigrantRepresentation ir = controladorPermis.getImmigrantRepresentation();
			controladorPermis.carregaAmfitrio(pr.getNIFAmfitrio());
			AmfitrioRepresentation ar = controladorPermis.getAmfitrioRepresentation();
			
			btnActualitza.setEnabled(true);
			btnActualitza_1.setEnabled(true);
			btnModifica.setEnabled(true);
			btnModifica_1.setEnabled(true);
			textFieldPassImm.setText(ir.getPassaport());
			textFieldFiliacioImm.setText(ir.getFiliacio());
			textFieldNacio.setText(ir.getNacio());
			
			textFieldFiliacioAmf.setText(ar.getFiliacio());
			textFieldDomAmf.setText(ar.getDomicili());
			textFieldNIF.setText(ar.getNif());
			textFieldPobAmf.setText(ar.getPoblacio());

			textFieldDomImm.setEnabled(true);
			textFieldDomImm.setEditable(true);
			textFieldDurada.setEnabled(true);
			textFieldDurada.setEditable(true);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error obtenint dades immigrant" ,JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
	private void mostrarInformacio(){
		textFieldFiliacioImm.setText(immi.getFiliacio());
		textFieldDomImm.setText(immi.getDomicili());
		textFieldNacio.setText(immi.getNacio());	
		textFieldFiliacioAmf.setText(amf.getFiliacio());
		textFieldDomAmf.setText(amf.getDomicili());
		textFieldPobAmf.setText(amf.getPoblacio());
	}	
	
	private void tancarFinestra(){
		vistaPrincipal.setEnabled(true);
		vistaPrincipal.setVisible(true);
		this.dispose();
	}
}
