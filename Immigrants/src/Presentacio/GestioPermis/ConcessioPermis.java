package Presentacio.GestioPermis;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import javax.swing.JComboBox;

import Aplicacio.ControladorGestioPermis;
import Aplicacio.Representations.AmfitrioRepresentation;
import Aplicacio.Representations.ImmigrantRepresentation;
import Presentacio.GestioAmfitrio.AltaAmfitrio;
import Presentacio.GestioAmfitrio.ModificacioAmfitrio;
import Presentacio.GestioImmigrant.AltaImmigrant;
import Presentacio.GestioImmigrant.ModificacioImmigrant;

@SuppressWarnings("serial")
public class ConcessioPermis extends JFrame {

	private JPanel contentPane;
	private JLabel lblConcessiDePerms;
	private JLabel lblDataConcessio;
	private JTextField textFieldDataConcessio;
	private JTextField textFieldDurada;
	private JButton buttonTornar;
	private JLabel lblNmeroDePassaport;
	private JTextField textFieldPassaport;
	private JLabel lblParentiu;
	private JComboBox<String> comboBox;
	private JLabel lblDomicili;
	private JTextField textFieldDomicili;
	private JLabel lblNifAmfitri;
	private JTextField textFieldNIF;
	private JButton btnCercarPass, btnModificar_1, btnModificar, btnActualitza, btnActualitza_1;
	private JButton buttonCercarNIF;
	private JButton buttonGuardar;
	private JButton buttonReset;
	private JFrame vistaPrincipal;
	private JTextField textFieldFiliImm;
	private JTextField textFieldNacio;
	private JTextField textFieldFiliAmf;
	private JTextField textFieldDomAmf;
	private JTextField textFieldPobAmf;
	private JLabel lblDomicili_2;
	private JTextField textFieldDomImm;
	
	private HashSet<String> parentius;
	private ControladorGestioPermis controladorPermis;
	ImmigrantRepresentation immi;
	AmfitrioRepresentation amf;
	
	public ConcessioPermis(JFrame vst) {
		this.vistaPrincipal = vst;
		try {
			controladorPermis = new ControladorGestioPermis();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error controlador",JOptionPane.ERROR_MESSAGE);
		}
		initComponents();
	}
	
	public ConcessioPermis(JFrame vst, String passaport) {
		this.vistaPrincipal = vst;
		try {
			controladorPermis = new ControladorGestioPermis();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error controlador",JOptionPane.ERROR_MESSAGE);
		}
		initComponents();
		this.textFieldPassaport.setText(passaport);
		btnCercarPassActionPerformed();
	}
	
	private void initComponents() {
		setTitle("Concessi� Perm�s de Resid�ncia");
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				tancarFinestra();
			}
		});
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 737, 558);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(null);
		setVisible(true);
		setLocationRelativeTo(null);
		
		this.lblConcessiDePerms = new JLabel("Concessi� de perm�s de resid�ncia");
		this.lblConcessiDePerms.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		this.lblConcessiDePerms.setBounds(38, 26, 285, 16);
		this.contentPane.add(this.lblConcessiDePerms);
		
		this.lblDataConcessio = new JLabel("Data Concessi�:");
		this.lblDataConcessio.setBounds(38, 385, 109, 16);
		this.contentPane.add(this.lblDataConcessio);
		
		this.textFieldDataConcessio = new JTextField();
		this.textFieldDataConcessio.setBounds(159, 382, 134, 22);
		this.contentPane.add(this.textFieldDataConcessio);
		this.textFieldDataConcessio.setColumns(10);

		Calendar c = new GregorianCalendar();
		textFieldDataConcessio.setText(Integer.toString(c.get(Calendar.DATE)) + "/" + Integer.toString(c.get(Calendar.MONTH)+1)
				+ "/" + Integer.toString(c.get(Calendar.YEAR)));
		textFieldDataConcessio.setEditable(false);
		
		JLabel lblDurada = new JLabel("Durada Concessi�:");
		lblDurada.setBounds(38, 432, 125, 16);
		this.contentPane.add(lblDurada);
		
		this.textFieldDurada = new JTextField();
		this.textFieldDurada.setEnabled(false);
		this.textFieldDurada.setBounds(159, 429, 134, 22);
		this.contentPane.add(this.textFieldDurada);
		this.textFieldDurada.setColumns(10);
		
		this.buttonTornar = new JButton("Tornar");
		this.buttonTornar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tancarFinestra();
			}
		});
		this.buttonTornar.setBounds(407, 486, 89, 23);
		this.contentPane.add(this.buttonTornar);
		
		this.lblNmeroDePassaport = new JLabel("Passaport:");
		this.lblNmeroDePassaport.setBounds(58, 60, 146, 16);
		this.contentPane.add(this.lblNmeroDePassaport);
		
		this.textFieldPassaport = new JTextField();
		this.textFieldPassaport.setBounds(135, 54, 134, 28);
		this.contentPane.add(this.textFieldPassaport);
		this.textFieldPassaport.setColumns(10);
		
		this.lblParentiu = new JLabel("Parentiu:");
		this.lblParentiu.setBounds(393, 432, 61, 16);
		this.contentPane.add(this.lblParentiu);
		
		this.comboBox = new JComboBox<String>();
		this.comboBox.setEnabled(false);
		this.comboBox.setBounds(464, 431, 132, 21);
		this.contentPane.add(this.comboBox);
		
		this.lblDomicili = new JLabel("Domicili:");
		this.lblDomicili.setBounds(393, 385, 61, 16);
		this.contentPane.add(this.lblDomicili);
		
		this.textFieldDomicili = new JTextField();
		this.textFieldDomicili.setEnabled(false);
		this.textFieldDomicili.setBounds(462, 382, 134, 22);
		this.contentPane.add(this.textFieldDomicili);
		this.textFieldDomicili.setColumns(10);
		
		this.lblNifAmfitri = new JLabel("NIF amfitri�:");
		this.lblNifAmfitri.setBounds(393, 60, 84, 16);
		this.contentPane.add(this.lblNifAmfitri);
		
		this.textFieldNIF = new JTextField();
		this.textFieldNIF.setEnabled(false);
		this.textFieldNIF.setBounds(487, 54, 109, 28);
		this.contentPane.add(this.textFieldNIF);
		this.textFieldNIF.setColumns(10);
		
		this.btnCercarPass = new JButton("Cercar");
		this.btnCercarPass.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnCercarPassActionPerformed();
			}
		});
		this.btnCercarPass.setBounds(271, 54, 75, 29);
		this.contentPane.add(this.btnCercarPass);
		
		this.buttonCercarNIF = new JButton("Cercar");
		this.buttonCercarNIF.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCercarNIFActionPerformed();
			}
		});
		this.buttonCercarNIF.setEnabled(false);
		this.buttonCercarNIF.setBounds(608, 55, 75, 29);
		this.contentPane.add(this.buttonCercarNIF);
		
		this.buttonGuardar = new JButton("Guardar");
		this.buttonGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonGuardarActionPerformed(e);
			}
		});
		this.buttonGuardar.setEnabled(false);
		this.buttonGuardar.setBounds(306, 486, 89, 23);
		this.contentPane.add(this.buttonGuardar);
		
		this.buttonReset = new JButton("Reset");
		this.buttonReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnReset();;
			}
		});
		this.buttonReset.setBounds(204, 486, 89, 23);
		this.contentPane.add(this.buttonReset);
		
		JLabel lblIntrodueixLesDades = new JLabel("Dades del nou perm�s de resid�ncia:");
		lblIntrodueixLesDades.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 13));
		lblIntrodueixLesDades.setBounds(38, 357, 292, 14);
		contentPane.add(lblIntrodueixLesDades);
		
		JPanel panelImm = new JPanel();
		panelImm.setBounds(38, 93, 661, 104);
		contentPane.add(panelImm);
		TitledBorder titleImmi;
		titleImmi = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black),"Informaci� de l'immigrant");
		panelImm.setBorder(titleImmi);
		panelImm.setLayout(null);
		
		JLabel lblFilImm = new JLabel("Filiaci�: ");
		lblFilImm.setBounds(12, 37, 73, 14);
		panelImm.add(lblFilImm);
		
		textFieldFiliImm = new JTextField();
		textFieldFiliImm.setEditable(false);
		textFieldFiliImm.setBounds(79, 34, 145, 20);
		panelImm.add(textFieldFiliImm);
		textFieldFiliImm.setColumns(10);
		
		JLabel lblNacio = new JLabel("Naci�: ");
		lblNacio.setBounds(472, 37, 46, 14);
		panelImm.add(lblNacio);
		
		textFieldNacio = new JTextField();
		textFieldNacio.setEditable(false);
		textFieldNacio.setBounds(528, 32, 116, 20);
		panelImm.add(textFieldNacio);
		textFieldNacio.setColumns(10);
		
		lblDomicili_2 = new JLabel("Domicili: ");
		lblDomicili_2.setBounds(239, 37, 69, 14);
		panelImm.add(lblDomicili_2);
		
		textFieldDomImm = new JTextField();
		textFieldDomImm.setEditable(false);
		textFieldDomImm.setBounds(308, 33, 145, 20);
		panelImm.add(textFieldDomImm);
		textFieldDomImm.setColumns(10);
		
		btnModificar = new JButton("Modificar");
		btnModificar.setEnabled(false);
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				modificarImmigrant();
			}
		});
		btnModificar.setBounds(407, 70, 106, 23);
		panelImm.add(btnModificar);
		
		btnActualitza = new JButton("Actualitza");
		btnActualitza.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnActualitzaImmi();
			}
		});
		btnActualitza.setEnabled(false);
		btnActualitza.setBounds(538, 70, 106, 23);
		panelImm.add(btnActualitza);
		
		JPanel panelAmf = new JPanel();
		panelAmf.setLayout(null);
		panelAmf.setBounds(38, 223, 661, 104);
		contentPane.add(panelAmf);
		TitledBorder titleAmf;
		titleAmf = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black),"Informaci� de l'amfitri�");
		panelAmf.setBorder(titleAmf);
		panelAmf.setLayout(null);		
		
		JLabel labelFilAmf = new JLabel("Filiaci�: ");
		labelFilAmf.setBounds(12, 37, 70, 14);
		panelAmf.add(labelFilAmf);
		
		textFieldFiliAmf = new JTextField();
		textFieldFiliAmf.setEditable(false);
		textFieldFiliAmf.setColumns(10);
		textFieldFiliAmf.setBounds(79, 35, 145, 20);
		panelAmf.add(textFieldFiliAmf);
		
		JLabel lblDomicili_1 = new JLabel("Domicili: ");
		lblDomicili_1.setBounds(239, 37, 64, 14);
		panelAmf.add(lblDomicili_1);
		
		textFieldDomAmf = new JTextField();
		textFieldDomAmf.setEditable(false);
		textFieldDomAmf.setBounds(308, 34, 145, 20);
		panelAmf.add(textFieldDomAmf);
		textFieldDomAmf.setColumns(10);
		
		JLabel lblPoblacio = new JLabel("Poblaci�: ");
		lblPoblacio.setBounds(465, 37, 67, 14);
		panelAmf.add(lblPoblacio);
		
		textFieldPobAmf = new JTextField();
		textFieldPobAmf.setEditable(false);
		textFieldPobAmf.setBounds(528, 33, 116, 20);
		panelAmf.add(textFieldPobAmf);
		textFieldPobAmf.setColumns(10);
		
		btnModificar_1 = new JButton("Modificar");
		btnModificar_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				modificarAmf();
			}
		});
		btnModificar_1.setEnabled(false);
		btnModificar_1.setBounds(407, 70, 106, 23);
		panelAmf.add(btnModificar_1);
		
		btnActualitza_1 = new JButton("Actualitza");
		btnActualitza_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnActualitza_1();
			}
		});
		btnActualitza_1.setEnabled(false);
		btnActualitza_1.setBounds(538, 70, 106, 23);
		panelAmf.add(btnActualitza_1);
	}
	
	protected void btnActualitza_1(){
		try {
			controladorPermis.carregaAmfitrio(textFieldNIF.getText());
			amf = controladorPermis.getAmfitrioRepresentation();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error al buscar l'immigrant",JOptionPane.ERROR_MESSAGE);
		}
		mostrarInformacio();
	}
	
	protected void btnActualitzaImmi(){
		try {
			controladorPermis.carregaImmigrant(textFieldPassaport.getText()); //busca immigrant a la bd, ex si no hi es
			immi = controladorPermis.getImmigrantRepresentation();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error al buscar l'immigrant",JOptionPane.ERROR_MESSAGE);
		}
		mostrarInformacio();
	}
	
	protected void tancarFinestra() {
		vistaPrincipal.setEnabled(true);
		this.dispose();
	}
	
	private void modificarImmigrant() {
		setEnabled(false);
		new ModificacioImmigrant(this, textFieldPassaport.getText());
	}
	 
	private	void modificarAmf() {
		setEnabled(false);
		new ModificacioAmfitrio(this, textFieldNIF.getText());
	}
	
	protected void btnCercarPassActionPerformed() {
		try {
			controladorPermis.carregaImmigrant(textFieldPassaport.getText()); //busca immigrant a la bd, ex si no hi es
			immi = controladorPermis.getImmigrantRepresentation();

			try {
				controladorPermis.carregaPermis(immi.getPassaport()); //busca el permis a la BD i el carrega, si no existeix EX
				//comprova si el permis està vigent retorna un string amb l'acci� a realitzar
				String accio = controladorPermis.permisVigent(); 

				if (accio.equals("renovar")){ //el permis esta en els ultims 11 dies o porta menys d11 dies caducat, pot renovar	
					try{ //cal comprovar que no s'hagi fet ja una renovacio en els dies anteriors
						String renovacioRecent = controladorPermis.renovacioVigent();
						if (renovacioRecent.equals("no")){
							JOptionPane.showMessageDialog(this, "L'immigrant ja t� una renovaci� vigent.", "Concessi� perm�s",JOptionPane.INFORMATION_MESSAGE);
						} else {
							JOptionPane.showMessageDialog(this, "L'immigrant introduit pot renovar el perm�s.", "Concessi� perm�s",JOptionPane.INFORMATION_MESSAGE);
							this.setEnabled(false);
							new RenovacioPermis(this.vistaPrincipal,textFieldPassaport.getText());
						}
					} catch (Exception e2){ //No te renovacions, a renovar el permis
						JOptionPane.showMessageDialog(this, "L'immigrant introduit pot renovar el perm�s.", "Concessi� perm�s",JOptionPane.INFORMATION_MESSAGE);
						this.dispose();
						this.setEnabled(false);
						new RenovacioPermis(this.vistaPrincipal,textFieldPassaport.getText());
					}
				} else if (accio.equals("comprobarRenovacions")){
					comprovarRenovacions(immi);
				} else if (accio.equals("no")){
					JOptionPane.showMessageDialog(this, "L'immigrant ja t� un perm�s vigent.", "Error concessi� perm�s",JOptionPane.ERROR_MESSAGE);
				}
			} catch (Exception e2){ //ha petat perque no te permis, pot fer concessio
				cercaPass();
			}
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(this, e1.getMessage(), "Error al buscar l'immigrant",JOptionPane.ERROR_MESSAGE);
			setEnabled(false);
			new AltaImmigrant(this, textFieldPassaport.getText());
		}
	}

	protected void buttonCercarNIFActionPerformed() {
		try {
			controladorPermis.carregaAmfitrio(textFieldNIF.getText());
			amf = controladorPermis.getAmfitrioRepresentation();
			cercarNIF();
			mostrarInformacio();
			getParentius();
			for (String p : parentius){
				comboBox.addItem(p);
			}
			buttonGuardar.setEnabled(true);
			buttonReset.setEnabled(true);
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(this, e1.getMessage(), "Error al buscar l'amfitri�",JOptionPane.ERROR_MESSAGE);
			new AltaAmfitrio(this, textFieldNIF.getText());
		}
	}
	
	private void getParentius() {
		try {
			parentius = controladorPermis.getParentius();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error al obtenir parentius",JOptionPane.ERROR_MESSAGE);
		}
		
	}

	protected void buttonGuardarActionPerformed(ActionEvent e){
		String durada = textFieldDurada.getText();
		String domicili = textFieldDomicili.getText();
		String parentiu = (String) comboBox.getSelectedItem();
		
		try {
			String numPermis = controladorPermis.concessioPermis(domicili, durada, parentiu);
			JOptionPane.showMessageDialog(this, "Perm�s concedit amb �xit! N�mero de perm�s: " + numPermis, "Concessi� realitzada",JOptionPane.INFORMATION_MESSAGE);
			tancarFinestra();
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(this, e1.getMessage(), "Error al realitzar la concessi� del perm�s",JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void comprovarRenovacions(ImmigrantRepresentation immi){
		try{
			//carrega renovacio, peta si no hi ha cap. comprova si la ultima renovacio esta vigent i torna l'accio a fer
			String accioRenovacio = controladorPermis.renovacioVigent();
			
			if (accioRenovacio.equals("renovar")){
				JOptionPane.showMessageDialog(this, "L'immigrant introduit pot renovar el perm�s.", "Concessi� perm�s",JOptionPane.INFORMATION_MESSAGE);
				this.dispose();
				new RenovacioPermis(this.vistaPrincipal,textFieldPassaport.getText());
	
			} else if (accioRenovacio.equals("concedir")){
				cercaPass();
			} else if (accioRenovacio.equals("no")){
				JOptionPane.showMessageDialog(this, "L'immigrant ja t� una renovaci� vigent.", "Error concessi� perm�s",JOptionPane.ERROR_MESSAGE);
			}
		} catch (Exception e1){ // no hi ha cap renovacio, donar permis
			cercaPass();
		}
	}
	
	protected void btnReset() {
		reset();
	}
		
	private void cercaPass(){
		textFieldNIF.setEnabled(true);
		textFieldNIF.setEditable(true);
		buttonCercarNIF.setEnabled(true);
		textFieldPassaport.setEditable(false);
		btnCercarPass.setEnabled(false);
	}
	
	private void mostrarInformacio(){
		textFieldFiliImm.setText(immi.getFiliacio());
		textFieldDomImm.setText(immi.getDomicili());
		textFieldNacio.setText(immi.getNacio());	
		textFieldFiliAmf.setText(amf.getFiliacio());
		textFieldDomAmf.setText(amf.getDomicili());
		textFieldPobAmf.setText(amf.getPoblacio());
	}	
	
	private void cercarNIF(){
		textFieldDomicili.setEnabled(true);
		comboBox.setEnabled(true);
		textFieldDurada.setEnabled(true);
		buttonGuardar.setEnabled(true);
		btnModificar_1.setEnabled(true);
		btnModificar.setEnabled(true);
		btnActualitza.setEnabled(true);
		btnActualitza_1.setEnabled(true);
		textFieldPassaport.setEnabled(true);
		textFieldPassaport.setEditable(false);
		textFieldNIF.setEnabled(true);
		textFieldNIF.setEditable(false);
		btnCercarPass.setEnabled(false);
		buttonCercarNIF.setEnabled(false);
	}
	
	private void reset(){
		textFieldPassaport.setText(null);
		textFieldNIF.setText(null);
		textFieldDurada.setText(null);
		textFieldDomicili.setText(null);
		textFieldFiliImm.setText(null);
		textFieldDomImm.setText(null);
		textFieldNacio.setText(null);
		textFieldFiliAmf.setText(null);
		textFieldDomAmf.setText(null);
		textFieldPobAmf.setText(null);
		textFieldDomicili.setEnabled(false);
		comboBox.setEnabled(false);
		textFieldDurada.setEnabled(false);
		buttonGuardar.setEnabled(false);
		btnModificar_1.setEnabled(false);
		btnModificar.setEnabled(false);
		textFieldPassaport.setEnabled(true);
		textFieldPassaport.setEditable(true);
		textFieldNIF.setEnabled(false);
		textFieldNIF.setEditable(false);
		btnCercarPass.setEnabled(true);
		buttonCercarNIF.setEnabled(false);
		btnActualitza.setEnabled(false);
		btnActualitza_1.setEnabled(false);
	}
	
	public void setPassword(String t){
		textFieldPassaport.setText(t);
		btnCercarPassActionPerformed();
	}
	
	public void setNIF(String t){
		textFieldNIF.setText(t);
		buttonCercarNIFActionPerformed();
		
	}
}
