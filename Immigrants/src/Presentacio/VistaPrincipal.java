package Presentacio;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import Aplicacio.ControladorLogin;
import Presentacio.Estadistica.EstadisticaAnual;
import Presentacio.GestioAmfitrio.AltaAmfitrio;
import Presentacio.GestioAmfitrio.ModificacioAmfitrio;
import Presentacio.GestioImmigrant.AltaImmigrant;
import Presentacio.GestioImmigrant.ModificacioImmigrant;
import Presentacio.GestioPermis.ConcessioPermis;
import Presentacio.GestioPermis.RenovacioPermis;
import Presentacio.Historials.HistorialAmfitrio;
import Presentacio.Historials.HistorialImmigrant;
import javax.swing.JLabel;
import java.awt.Component;

@SuppressWarnings("serial")
public class VistaPrincipal extends JFrame {
	private JPanel contentPane;
	private JButton btnSortir;
	private ControladorLogin ControladorLogin;
	private JButton btnNouPermis;
	private JButton btnRenovacioPermis;
	private String user;

	public VistaPrincipal(ControladorLogin c, String user) throws Exception {
		setVisible(true);
		ControladorLogin = c;
		this.user=user;
		initComponents();
	}
	
	private void initComponents() {
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent arg0) {
				tancarFinestra();
			}
		});
		setResizable(false);
		setTitle("TCM Immigrants");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 904, 499);
		setLocationRelativeTo(null);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(null);
		
		btnSortir = new JButton("Sortir");
		btnSortir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				tancarFinestra();
			}
		});
		btnSortir.setBounds(768, 424, 89, 23);
		contentPane.add(btnSortir);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 898, 21);
		contentPane.add(menuBar);
		
		JMenu mnAmfitri = new JMenu("Amfitri�");
		mnAmfitri.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		menuBar.add(mnAmfitri);
		
		JMenuItem mntmAlta = new JMenuItem("Alta Amfitri�");
		mntmAlta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnAltaAmfitrio();
			}
		});
		mnAmfitri.add(mntmAlta);
		
		JMenuItem mntmModificar = new JMenuItem("Modifica Amfitri�");
		mntmModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnModAmfitrio();
			}
		});
		mnAmfitri.add(mntmModificar);
		
		JMenu mnImmigrant = new JMenu("Immigrant");
		mnImmigrant.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		menuBar.add(mnImmigrant);
		
		JMenuItem mntmAlta_1 = new JMenuItem("Alta Immigrant");
		mntmAlta_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnAltaImmigrant();
			}
		});
		mnImmigrant.add(mntmAlta_1);
		
		JMenuItem mntmModifica = new JMenuItem("Modifica Immigrant");
		mntmModifica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnModImmigrant();
			}
		});
		mnImmigrant.add(mntmModifica);
		
		JMenu mnPerms = new JMenu("Perm�s");
		mnPerms.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		menuBar.add(mnPerms);
		
		JMenuItem mntmConcedirPerms = new JMenuItem("Concedir Perm�s");
		mnPerms.add(mntmConcedirPerms);
		
		JMenuItem mntmRenovaciPerms = new JMenuItem("Renovaci� Perm�s");
		mnPerms.add(mntmRenovaciPerms);
		mntmRenovaciPerms.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnRenovacioPermis();
			}
		});
		mntmConcedirPerms.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnNouPermis();
			}
		});
		
		JMenu mnHistorial = new JMenu("Historial");
		mnHistorial.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		menuBar.add(mnHistorial);
		
		JMenuItem mntmAmfitri = new JMenuItem("Historial Amfitri�");
		mntmAmfitri.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnHistorialAmfitrio();
			}
		});
		mnHistorial.add(mntmAmfitri);
		
		JMenuItem mntmImmigrant = new JMenuItem("Historial Immigrant");
		mntmImmigrant.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnHistorialImmigrant();
			}
		});
		mnHistorial.add(mntmImmigrant);
		
		JMenu mnEstadstica = new JMenu("Estad�stica");
		mnEstadstica.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		menuBar.add(mnEstadstica);
		
		JMenuItem mntmEstadsticaAnual = new JMenuItem("Estad�stica Anual");
		mntmEstadsticaAnual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnEstadisticaAnual();
			}
		});
		mnEstadstica.add(mntmEstadsticaAnual);
		
		Component glue = Box.createGlue();
		menuBar.add(glue);
		
		JMenu menuUser = new JMenu(user);
		menuUser.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		menuBar.add(menuUser);
				
		
		JMenuItem mntmTancaSessio = new JMenuItem("Tancar sessi�");
		mntmTancaSessio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnTancaSessio();
			}
		});
		menuUser.add(mntmTancaSessio);
		
		JMenuItem mntmSortir = new JMenuItem("Sortir");
		mntmSortir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tancarFinestra();
			}
		});
		menuUser.add(mntmSortir);
		
		JPanel panelImmi = new JPanel();
		panelImmi.setBounds(35, 68, 233, 315);
		contentPane.add(panelImmi);
		panelImmi.setLayout(null);
		
		TitledBorder titleImmi;
		titleImmi = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black),"Gesti� immigrants");
		panelImmi.setBorder(titleImmi);
		
		TitledBorder titleAmf;
		titleAmf = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black),"Gesti� amfitrions");
		
		TitledBorder titleHist;
		titleHist = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black),"Historials");
		
		JButton AltaImmi = new JButton("Nou Immigrant");
		AltaImmi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnAltaImmigrant();
			}
		});
		AltaImmi.setBounds(24, 31, 187, 38);
		panelImmi.add(AltaImmi);
		
		JButton ModImmi = new JButton("Modifica Immigrant");
		ModImmi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnModImmigrant();
			}
		});
		ModImmi.setBounds(24, 91, 187, 38);
		panelImmi.add(ModImmi);
		
		this.btnNouPermis = new JButton("Crear Permis");
		this.btnNouPermis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnNouPermis();
			}
		});
		this.btnNouPermis.setBounds(24, 152, 187, 38);
		panelImmi.add(this.btnNouPermis);
		
		this.btnRenovacioPermis = new JButton("Renovar Permis");
		this.btnRenovacioPermis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnRenovacioPermis();
			}
		});
		this.btnRenovacioPermis.setBounds(24, 213, 187, 38);
		panelImmi.add(this.btnRenovacioPermis);
		
		JPanel panelAmf = new JPanel();
		panelAmf.setBounds(325, 68, 233, 315);
		panelAmf.setBorder(titleAmf);
		panelAmf.setLayout(null);
		contentPane.add(panelAmf);
		
		JButton ModAmf = new JButton("Modifica Amfitri�");
		ModAmf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnModAmfitrio();
			}
		});
		ModAmf.setBounds(25, 93, 187, 38);
		panelAmf.add(ModAmf);
		
		JButton AltaAmf = new JButton("Nou Amfitri�");
		AltaAmf.setBounds(25, 30, 187, 38);
		panelAmf.add(AltaAmf);
		
		JPanel panel = new JPanel();
		panel.setBounds(624, 68, 233, 315);
		panel.setBorder(titleHist);
		panel.setLayout(null);
		contentPane.add(panel);
		
		JButton btnHistImm = new JButton("Historial Immigrant");
		btnHistImm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnHistorialImmigrant();
			}
		});
		btnHistImm.setBounds(25, 93, 187, 38);
		panel.add(btnHistImm);
		
		JButton btnHistAmf = new JButton("Historial Amfitri�");
		btnHistAmf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnHistorialAmfitrio();
			}
		});
		btnHistAmf.setBounds(25, 30, 187, 38);
		panel.add(btnHistAmf);
		
		JButton btnNewButton = new JButton("Estad�stica Anual");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnEstadisticaAnual();
			}
		});
		btnNewButton.setBounds(25, 158, 187, 38);
		panel.add(btnNewButton);
		AltaAmf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnAltaAmfitrio();
			}
		});
	}

	//PRIVATE METHODS
	private void btnTancaSessio() {
		this.dispose();
		new Login();
	}
	
	private void btnEstadisticaAnual() {
		this.setEnabled(false);
		new EstadisticaAnual(this);
	}
	
	private void btnModAmfitrio() {
		this.setEnabled(false);
		new ModificacioAmfitrio(this);
	}

	private void btnAltaAmfitrio() {
		this.setEnabled(false);
		new AltaAmfitrio(this);
	}

	private void btnHistorialAmfitrio(){
		this.setEnabled(false);
		new HistorialAmfitrio(this);
	}
	
	private void btnHistorialImmigrant(){
		this.setEnabled(false);
		new HistorialImmigrant(this);
	}
	
	private void btnModImmigrant() {
		this.setEnabled(false);
		new ModificacioImmigrant(this);
	}

	private void btnAltaImmigrant() {
		this.setEnabled(false);
		new AltaImmigrant(this);
	}

	private void tancarFinestra(){
		try {
			ControladorLogin.close();
			this.dispose();
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this, "Error al tancar la connexio SQL");
		}
	}
	
	private void btnNouPermis() {
		this.setEnabled(false);
		new ConcessioPermis(this);
	}
	
	private void btnRenovacioPermis() {
		this.setEnabled(false);
		new RenovacioPermis(this);
	}
}
