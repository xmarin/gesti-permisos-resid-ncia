package Presentacio.GestioImmigrant;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;

import Aplicacio.ControladorGestioImmigrant;
import Aplicacio.Representations.ImmigrantRepresentation;
import Presentacio.VistaPrincipal;

@SuppressWarnings("serial")
public class ModificacioImmigrant extends JFrame {
	private JPanel contentPane;
	private JLabel lblPassaport;
	private JTextField textFieldPassaport;
	private JLabel lblNom;
	private JTextField textFieldNom;
	private JLabel lblPrimerCognom;
	private JTextField textFieldDomicili;
	private JLabel lblSegonCognom;
	private JTextField textFieldPoblacio;
	private JLabel lblNacio;
	private JComboBox<String> comboBoxNacio;
	private ControladorGestioImmigrant CI;
	private JFrame fr;
	private JButton btnCancelar;
	private HashMap<String,String> HMnacions;
	private ArrayList<String> nacions;
	private JButton btnGuardar, btnBuscar, btnReset;
	private String passaport;
	
	/**
	 * @wbp.parser.constructor
	 */
	public ModificacioImmigrant(JFrame f){
		fr = f;
		
		try {
			CI = new ControladorGestioImmigrant();
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(this, e1.getMessage(), "Error Gesti� Immigrant",
			        JOptionPane.ERROR_MESSAGE);
		}
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				tancarFinestra();
			}
		});
		
		initComponents();
	}

	public ModificacioImmigrant (JFrame f, String Passaport){
		fr = f;
		passaport = Passaport;
		
		try {
			CI = new ControladorGestioImmigrant();
			CI.carregaImmigrant(passaport);
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(this, e1.getMessage(), "Error Gesti� Immigrant",
			        JOptionPane.ERROR_MESSAGE);
		}
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				tancarFinestra();
			}
		});
		
		initComponents();
		
		btnBuscar.setEnabled(false);
		textFieldPassaport.setEnabled(false);
		
		try {
			ImmigrantRepresentation i = CI.getImmigrantRepresentation(passaport);
			textFieldNom.setText(i.getFiliacio());
			textFieldDomicili.setText(i.getDomicili());
			textFieldPassaport.setText(i.getPassaport());
			textFieldPoblacio.setText(i.getPoblacio());
			comboBoxNacio.setSelectedIndex(nacions.indexOf(HMnacions.get(i.getCodiNacio())));
			activarCamps(true);
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(this, e1.getMessage(), "Error Gesti� Immigrant",
			        JOptionPane.ERROR_MESSAGE);
		}
		btnCancelar.setEnabled(true);
	}
	
	protected void tancarFinestra() {
		fr.setEnabled(true);
		dispose();
	}

	private void initComponents() {
		setTitle("Modificaci� Immigrant");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 340, 297);
		setLocationRelativeTo(null);
		
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(null);
		setVisible(true);
		
		this.lblPassaport = new JLabel("Passaport:");
		this.lblPassaport.setBounds(21, 17, 65, 16);
		this.contentPane.add(this.lblPassaport);
		
		this.textFieldPassaport = new JTextField();
		this.textFieldPassaport.setBounds(121, 11, 96, 29);
		this.contentPane.add(this.textFieldPassaport);
		this.textFieldPassaport.setColumns(10);
		
		this.lblNom = new JLabel("Filiaci�:");
		this.lblNom.setBounds(21, 56, 65, 16);
		this.contentPane.add(this.lblNom);
		
		this.textFieldNom = new JTextField();
		textFieldNom.setEnabled(false);
		this.textFieldNom.setBounds(121, 50, 183, 28);
		this.contentPane.add(this.textFieldNom);
		this.textFieldNom.setColumns(10);
		
		this.lblPrimerCognom = new JLabel("Domicili: ");
		this.lblPrimerCognom.setBounds(21, 95, 102, 16);
		this.contentPane.add(this.lblPrimerCognom);
		
		this.textFieldDomicili = new JTextField();
		textFieldDomicili.setEnabled(false);
		this.textFieldDomicili.setBounds(121, 89, 183, 28);
		this.contentPane.add(this.textFieldDomicili);
		this.textFieldDomicili.setColumns(10);
		
		this.lblSegonCognom = new JLabel("Poblaci�:");
		this.lblSegonCognom.setBounds(21, 134, 102, 16);
		this.contentPane.add(this.lblSegonCognom);
		
		this.textFieldPoblacio = new JTextField();
		textFieldPoblacio.setEnabled(false);
		this.textFieldPoblacio.setBounds(121, 128, 183, 28);
		this.contentPane.add(this.textFieldPoblacio);
		this.textFieldPoblacio.setColumns(10);
		
		this.lblNacio = new JLabel("Naci�:");
		this.lblNacio.setBounds(21, 174, 61, 16);
		this.contentPane.add(this.lblNacio);
		
		this.comboBoxNacio = new JComboBox<String>();
		comboBoxNacio.setEnabled(false);
		comboBoxNacio.setEditable(false);
		this.comboBoxNacio.setBounds(121, 167, 96, 27);
		this.contentPane.add(this.comboBoxNacio);
		
		try {
			HMnacions = CI.getNacions();
			nacions = new ArrayList<String>();
			
			for (String s : HMnacions.keySet()){
				nacions.add(HMnacions.get(s).toString());
			}
			Collections.sort(nacions);
			for (int i=0; i<nacions.size(); i++){
				comboBoxNacio.addItem(nacions.get(i));
			}
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(new JFrame(), 
					e1.getMessage(), 
					"No s'han pogut obtenir les nacions", 
					JOptionPane.ERROR_MESSAGE);
		}
		
		
		this.btnGuardar = new JButton("Guardar");
		btnGuardar.setEnabled(false);
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnModificaImmigrant();
			}
		});
		this.btnGuardar.setBounds(116, 220, 96, 29);
		this.contentPane.add(this.btnGuardar);
		
		btnCancelar = new JButton("Tornar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				tancarFinestra();
			}
		});
		btnCancelar.setBounds(222, 220, 92, 29);
		contentPane.add(btnCancelar);
		
		btnBuscar = new JButton("Cercar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				carregarImmigrant();
			}
		});
		btnBuscar.setBounds(227, 14, 76, 29);
		contentPane.add(btnBuscar);

		btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				resetDades();
			}
		});
		btnReset.setBounds(10, 220, 96, 29);
		contentPane.add(btnReset);
		
		activarCamps(false);
	}

	protected void resetDades() {
		btnBuscar.setEnabled(true);
		btnGuardar.setEnabled(false);
		btnCancelar.setEnabled(true);
		textFieldPassaport.setEnabled(true);
		textFieldPassaport.setText(null);
		textFieldNom.setText(null);
		textFieldNom.setEnabled(false);
		textFieldDomicili.setText(null);
		textFieldDomicili.setEnabled(false);
		textFieldPoblacio.setText(null);
		textFieldPoblacio.setEnabled(false);
		comboBoxNacio.setEditable(false);
		comboBoxNacio.setEnabled(false);
	}

	protected void btnModificaImmigrant() {
		try{
			CI.modificarImmigrant(getKeyForValue(nacions.get(comboBoxNacio.getSelectedIndex())), textFieldNom.getText(), textFieldDomicili.getText(), textFieldPoblacio.getText());
			JOptionPane.showMessageDialog(this, 
					"Immigrant amb passaport " + textFieldPassaport.getText() + " modificat correctament.", 
					"Modificacio amb �xit", 
					JOptionPane.INFORMATION_MESSAGE);
			fr.setEnabled(true);
			this.dispose();
		}catch (Exception e){
			JOptionPane.showMessageDialog(new JFrame(), 
					e.getMessage(), 
					"Error modificar immigrant", 
					JOptionPane.ERROR_MESSAGE);
		}
	}

	private void carregarImmigrant() {
		try {
			ImmigrantRepresentation imm = CI.getImmigrantRepresentation(textFieldPassaport.getText());
			activarCamps(true);
			textFieldNom.setText(imm.getFiliacio());
			textFieldDomicili.setText(imm.getDomicili());
			textFieldPoblacio.setText(imm.getPoblacio());
			comboBoxNacio.setSelectedIndex(nacions.indexOf(HMnacions.get(imm.getCodiNacio())));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Immigrant inexistent",
			        JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void activarCamps(boolean b){
		textFieldPassaport.setEnabled(!b);
		btnBuscar.setEnabled(!b);
		textFieldNom.setEnabled(b);
		textFieldDomicili.setEnabled(b);
		textFieldPoblacio.setEnabled(b);
		btnGuardar.setEnabled(b);
	}
	
	private String getKeyForValue(String value) {
	    for (Entry<String, String> entry : HMnacions.entrySet()) {
	         if (entry.getValue().equals(value)) {
	             return entry.getKey();
	         }
	     }
	    return null;
	}
}
