package Presentacio.GestioImmigrant;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;

import Aplicacio.ControladorGestioImmigrant;
import Presentacio.GestioPermis.ConcessioPermis;

@SuppressWarnings("serial")
public class AltaImmigrant extends JFrame {
	private JPanel contentPane;
	private JLabel lblPassaport;
	private JTextField textFieldPassaport;
	private JLabel lblFiliacio;
	private JTextField textFieldFiliacio;
	private JLabel lblDomicili;
	private JTextField textFieldDomicili;
	private JLabel lblPoblacio;
	private JTextField textFieldPoblacio;
	private JLabel lblNacio;
	private JComboBox<String> comboBoxNacio;
	private JButton btnGuardar;
	private ControladorGestioImmigrant c;
	private JFrame vp;
	private HashMap<String,String> HMnacions;
	private ArrayList<String> nacions;

	
	public AltaImmigrant(JFrame f) {
		vp = f;
		this.setVisible(true);
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				tancarFinestra();
			}
		});
		
		try {
			c = new ControladorGestioImmigrant();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, 
					e.getMessage(), 
					"Error Alta Immigrant", 
					JOptionPane.ERROR_MESSAGE);
		}
		initComponents();
	}
	
	public AltaImmigrant(JFrame f, String passaport) {
		vp = f;
		this.setVisible(true);
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				tancarFinestra();
			}
		});
		
		try {
			c = new ControladorGestioImmigrant();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, 
					e.getMessage(), 
					"Error Alta Immigrant", 
					JOptionPane.ERROR_MESSAGE);
		}
		initComponents();
		textFieldPassaport.setText(passaport);
	}

	protected void tancarFinestra() {
		vp.setEnabled(true);
		dispose();
	}

	private void initComponents() {
		setTitle("Alta Immigrant");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 340, 297);
		setLocationRelativeTo(null);
		
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(null);
		
		this.lblPassaport = new JLabel("Passaport:");
		this.lblPassaport.setBounds(21, 17, 65, 16);
		this.contentPane.add(this.lblPassaport);
		
		this.textFieldPassaport = new JTextField();
		this.textFieldPassaport.setBounds(121, 11, 102, 28);
		this.contentPane.add(this.textFieldPassaport);
		this.textFieldPassaport.setColumns(10);
		
		this.lblFiliacio = new JLabel("Filiaci�:");
		this.lblFiliacio.setBounds(21, 56, 65, 16);
		this.contentPane.add(this.lblFiliacio);
		
		this.textFieldFiliacio = new JTextField();
		this.textFieldFiliacio.setBounds(121, 50, 183, 28);
		this.contentPane.add(this.textFieldFiliacio);
		this.textFieldFiliacio.setColumns(10);
		
		this.lblDomicili = new JLabel("Domicili: ");
		this.lblDomicili.setBounds(21, 95, 102, 16);
		this.contentPane.add(this.lblDomicili);
		
		this.textFieldDomicili = new JTextField();
		this.textFieldDomicili.setBounds(121, 89, 183, 28);
		this.contentPane.add(this.textFieldDomicili);
		this.textFieldDomicili.setColumns(10);
		
		this.lblPoblacio = new JLabel("Poblaci�: ");
		this.lblPoblacio.setBounds(21, 134, 102, 16);
		this.contentPane.add(this.lblPoblacio);
		
		this.textFieldPoblacio = new JTextField();
		this.textFieldPoblacio.setBounds(121, 128, 183, 28);
		this.contentPane.add(this.textFieldPoblacio);
		this.textFieldPoblacio.setColumns(10);
		
		this.lblNacio = new JLabel("Naci�:");
		this.lblNacio.setBounds(21, 174, 61, 16);
		this.contentPane.add(this.lblNacio);
		
		this.comboBoxNacio = new JComboBox<String>();
		this.comboBoxNacio.setBounds(121, 167, 96, 27);
		
		try {
			HMnacions = c.getNacions();
			nacions = new ArrayList<String>();
			
			for (String s : HMnacions.keySet()){
				nacions.add(HMnacions.get(s).toString());
			}
			Collections.sort(nacions);
			for (int i=0; i<nacions.size(); i++){
				comboBoxNacio.addItem(nacions.get(i));
			}
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(this, 
					e1.getMessage(), 
					"No s'han pogut obtenir les nacions", 
					JOptionPane.ERROR_MESSAGE);
		}
		
		this.contentPane.add(this.comboBoxNacio);
		
		this.btnGuardar = new JButton("Guardar");
		this.btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnGuardar();
			}
		});
		this.btnGuardar.setBounds(116, 220, 96, 29);
		this.contentPane.add(this.btnGuardar);
		
		JButton btnTornar = new JButton("Tornar");
		btnTornar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tancarFinestra();
			}
		});
		btnTornar.setBounds(222, 220, 92, 29);
		contentPane.add(btnTornar);
		
		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnResetDades();
			}
		});
		btnReset.setBounds(10, 220, 96, 29);
		contentPane.add(btnReset);
	}
	protected void btnResetDades() {
		textFieldPassaport.setText(null);
		textFieldDomicili.setText(null);
		textFieldFiliacio.setText(null);
		textFieldPoblacio.setText(null);
	}

	protected void btnGuardar() {
		String passaport = textFieldPassaport.getText();
		String filiacio = textFieldFiliacio.getText();
		String domicili = textFieldDomicili.getText();
		String poblacio = textFieldPoblacio.getText();
		String nacio = getKeyForValue(nacions.get(comboBoxNacio.getSelectedIndex()));
		
		try {
			c.afegirImmigrant(passaport, nacio, filiacio, domicili, poblacio);
			JOptionPane.showMessageDialog(this, "Immigrant afegit amb �xit.", "Afegir",JOptionPane.INFORMATION_MESSAGE);
			if (vp instanceof ConcessioPermis) ((ConcessioPermis) vp).setPassword(passaport);
			tancarFinestra();
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(this, e1.getMessage(), "Error afegint immigrant",JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private String getKeyForValue(String value) {
	    for (Entry<String, String> entry : HMnacions.entrySet()) {
	         if (entry.getValue().equals(value)) {
	             return entry.getKey();
	         }
	     }
	    return null;
	}
}
