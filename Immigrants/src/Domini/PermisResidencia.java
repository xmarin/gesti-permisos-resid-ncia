package Domini;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class PermisResidencia {
	
	private int numPermis;
	private Amfitrio amfitrio;
	private Immigrant immigrant;
	private Date dataConcessio;
	private int duradaConcessio;
	private String domicili;
	private Parentiu parentiu;
	
	public PermisResidencia(int numPermis, Amfitrio amf, Immigrant imm, Date dataConcessio, String duradaConcessio, String domicili, String parentiu) throws Exception {
		setNumPermis(numPermis);
		setDuradaConcessio(duradaConcessio);
		setParentiu(parentiu);
		setDomicili(domicili);
		setAmfitrio(amf);
		setImmigrant(imm);
		setDataConcessio(dataConcessio);
	}

	public int getNumPermis() { return numPermis;}
	public Date getDataConcessio() { return dataConcessio;}
	public int getDuradaConcessio() { return duradaConcessio;}
	public Amfitrio getAmfitrio() { return amfitrio;}
	public Immigrant getImmigrant() { return immigrant;}
	public String getDomicili() { return domicili;}
	public Parentiu getParentiu() { return parentiu;}
	
	public void setNumPermis(int numPermis) throws Exception {
		if (numPermis < 0) throw new Exception("N�mero de perm�s no v�lid.");
		this.numPermis = numPermis;
	}
	
	public void setDataConcessio(Date data) throws Exception {
		if(data == null) throw new Exception("La data no pot estar buida.");
		this.dataConcessio = data;
	}
	
	public Date stringToDate(String data) throws Exception{
		if (data.trim().isEmpty()) throw new Exception("Data buida.");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try{
			return sdf.parse(data);
		}catch(ParseException e){
			e.getMessage();
		}
		return null;
	}
	
	public void setAmfitrio(Amfitrio amf) throws Exception{
		if (amf == null) throw new Exception("Amfitrio no v�lid.");
		amfitrio = amf;
	}
	
	public void setImmigrant(Immigrant immi) throws Exception{
		if (immi == null) throw new Exception("Immigrant no v�lid.");
		immigrant = immi;
	}

	public void setDuradaConcessio(String duradaConcessio) throws Exception {
		if (duradaConcessio.trim().isEmpty()) throw new Exception("Durada de concessi� no pot estar buida.");
		try{
			this.duradaConcessio = Integer.parseInt(duradaConcessio);
		} catch (NumberFormatException e){
			throw new Exception("La durada ha de ser num�rica.");
		}
	}

	public void setParentiu(String parentiu) throws Exception {
		this.parentiu = new Parentiu(parentiu); //ja valida la classe Parentiu
	}
	
	public void setDomicili(String domicili) throws Exception {
		if (domicili.trim().isEmpty()) throw new Exception("El domicili no pot estar buit.");
		if (domicili.length() > 40) throw new Exception("Mida del domicili massa gran.");
		this.domicili = domicili;
	}
	
	public String permisVigent() throws Exception{
		GregorianCalendar dataCaducitat = getDataCaducitat(); //data concessio + durada
		
		GregorianCalendar dataActual = new GregorianCalendar(); //data actual
		dataActual.getInstance();
		
		if (dataActual.before(dataCaducitat)){
			if (potRenovar()) return "renovar";
			else return "no";
		} else {
			if(potRenovar()) return "renovar";
			else return "comprobarRenovacions";
		}
		
	}

	public boolean potRenovar(){
		GregorianCalendar dataCaducitat = getDataCaducitat();
		
		GregorianCalendar dataActualDies2 = new GregorianCalendar();
		dataActualDies2.getInstance();
		dataActualDies2.add(dataActualDies2.DAY_OF_MONTH, 10);
		dataActualDies2.set(dataActualDies2.HOUR_OF_DAY, 0);
		dataActualDies2.set(dataActualDies2.MINUTE, 0);
		dataActualDies2.set(dataActualDies2.SECOND, 0);
		
		GregorianCalendar dataActualDies1 = new GregorianCalendar();
		dataActualDies1.getInstance();
		dataActualDies1.add(dataActualDies1.DAY_OF_MONTH, -11);
		dataActualDies1.set(dataActualDies1.HOUR_OF_DAY, 0);
		dataActualDies1.set(dataActualDies1.MINUTE, 0);
		dataActualDies1.set(dataActualDies1.SECOND, 0);

		if (dataActualDies1.before(dataCaducitat) && dataActualDies2.after(dataCaducitat)){
			return true;
		} else {
			return false;
		}
	}
		
	public GregorianCalendar getDataCaducitat(){
		GregorianCalendar dataCaducitat = new GregorianCalendar();
		dataCaducitat.setTimeInMillis(dataConcessio.getTime());
		dataCaducitat.add(dataCaducitat.MONTH, duradaConcessio);
		return dataCaducitat;
	}
	
}
