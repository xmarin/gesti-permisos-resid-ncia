package Domini;

public class Continent {
	
	private String nomContinent;
	
	public Continent(String nomContinent) throws Exception{
		setNomContinent(nomContinent);
	}

	public String getNomContinent() { return nomContinent;}

	public void setNomContinent(String nomContinent) throws Exception {
		if (nomContinent.trim().isEmpty()) throw new Exception("Nom continent no pot estar buit.");
		if (nomContinent.length() > 40) throw new Exception("Nom continent massa gran.");
		this.nomContinent = nomContinent;
	}
	
	

}
