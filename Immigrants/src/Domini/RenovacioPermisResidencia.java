package Domini;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class RenovacioPermisResidencia {
	
	private int numPermis, duradaRenovacio;
	private Date dataRenovacio;
	private String domicili;
	
	public RenovacioPermisResidencia(int numPermis, Date data, String duradaConcessio, String domicili) throws Exception{
		setNumPermis(numPermis);
		setDataRenovacio(data);
		setDuradaRenovacio(duradaConcessio);
		setDomicili(domicili);
	}

	public int getNumPermis() { return numPermis;}
	public int getDuradaRenovacio() { return duradaRenovacio;}
	public Date getDataRenovacio() { return dataRenovacio;}
	public String getDomicili() { return domicili;}

	public void setNumPermis(int numPermis) throws Exception {
		if (numPermis < 0) throw new Exception("N�mero de perm�s no v�lid.");
		this.numPermis = numPermis;
	}
	
	public void setDataRenovacio(Date data) throws Exception {
		if(data == null) throw new Exception("La data no pot estar buida.");
		this.dataRenovacio = data;
	}
	
	public void stringToDate(String data) throws Exception{
		if (data.trim().isEmpty()) throw new Exception("Data buida.");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try{
			dataRenovacio = sdf.parse(data);
		}catch(ParseException e){
			e.getMessage();
		}
	}

	public void setDuradaRenovacio(String durada) throws Exception {
		if (durada.trim().isEmpty()) throw new Exception("Durada de concessi� no pot estar buida.");
		
		try{
			int num = Integer.parseInt(durada);
			if (num < 0 ) throw new Exception("La durada de la renovaci� no pot ser negativa");
			this.duradaRenovacio =  num;
		} catch (NumberFormatException e){
			throw new Exception("La durada ha de ser num�rica.");
		}
	}
	
	public void setDomicili(String domicili) throws Exception {
		if (domicili.trim().isEmpty()) throw new Exception("El domicili no pot estar buit.");
		if (domicili.length() > 40) throw new Exception("Mida del domicili massa gran.");
		this.domicili = domicili;
	}
	
	public String renovacioVigent(){
		GregorianCalendar dataCaducitat = getDataCaducitat(); //data concessio + durada
		
		GregorianCalendar dataActual = new GregorianCalendar(); //data actual
		dataActual.getInstance();
		
		if (dataActual.before(dataCaducitat)){
			if (potRenovar()) return "renovar";
			else return "no";
		} else {
			if(potRenovar()) return "renovar";
			else return "concedir";
		}
	}
		
	public boolean potRenovar(){
		GregorianCalendar dataCaducitat = getDataCaducitat();
		
		GregorianCalendar dataActualDies2 = new GregorianCalendar();
		dataActualDies2.getInstance();
		dataActualDies2.add(dataActualDies2.DAY_OF_MONTH, 10);
		dataActualDies2.set(dataActualDies2.HOUR_OF_DAY, 0);
		dataActualDies2.set(dataActualDies2.MINUTE, 0);
		dataActualDies2.set(dataActualDies2.SECOND, 0);

		GregorianCalendar dataActualDies1 = new GregorianCalendar();
		dataActualDies1.getInstance();
		dataActualDies1.add(dataActualDies1.DAY_OF_MONTH, -11);
		dataActualDies1.set(dataActualDies1.HOUR_OF_DAY, 0);
		dataActualDies1.set(dataActualDies1.MINUTE, 0);
		dataActualDies1.set(dataActualDies1.SECOND, 0);
		
		if (dataActualDies1.before(dataCaducitat) && dataActualDies2.after(dataCaducitat)){
			return true;
		} else {
			return false;
		}
	}
			
	public GregorianCalendar getDataCaducitat(){
		GregorianCalendar dataCaducitat = new GregorianCalendar();
		dataCaducitat.setTimeInMillis(dataRenovacio.getTime());
		dataCaducitat.add(dataCaducitat.MONTH, duradaRenovacio);
		return dataCaducitat;
	}	
}