package Domini;

public class Parentiu {
	
	private String tipus;
	
	public Parentiu(String tipus) throws Exception {
		setTipus(tipus);
	}
	
	public String getTipus() { return tipus;}

	public void setTipus(String tipus) throws Exception {
		if (tipus.trim().isEmpty()) throw new Exception("El tipus de parentiu est� buit.");
		if (tipus.length() > 10) throw new Exception("El tipus de parentiu es massa llarg.");
		this.tipus = tipus;
	}

	public String toString(){
		return this.tipus;
	}
}
