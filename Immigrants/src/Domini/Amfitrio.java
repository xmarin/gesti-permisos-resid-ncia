package Domini;

public class Amfitrio extends Persona{

	private String nif;
	private static final String lletres = "TRWAGMYFPDXBNJZSQVHLCKE";
	
	public Amfitrio(String nif, String domicili, String filiacio, String poblacio) throws Exception {
		super(filiacio, domicili, poblacio);
		setNif(nif);
	}

	public void setNif(String nif) throws Exception {
		nif = nif.toUpperCase();
		if (!verificar(nif)) throw new Exception("NIF: Lletra incorrecta");
		this.nif = nif;
	}

	public boolean verificar(String dniUp) throws Exception {
		int numDNI;
		boolean resultat = false;

		if (dniUp.length() == 9) {
			try{
				numDNI = Integer.parseInt(dniUp.substring(0, 8));
			}catch(IllegalArgumentException e){
				throw new Exception("NIF: No s'admeten lletres a la part num�rica.");
			}
			int valor = numDNI % 23;
			char lletraDNI = dniUp.charAt(8);
			char lletraCalc = lletres.charAt(valor);

			if (lletraDNI == lletraCalc) {
				resultat = true;
			}
		}else{
			throw new Exception("NIF: Longitud incorrecta");
		}
		return resultat;
	}
	
	public String getNif() { 
		return nif;
	}

	public String toString() {
		return getFiliacio();
	}

	
	

}
