package Domini;

public class Nacio {
	
	private String codiNacio;
	private String nom;
	private ZonaGeografica zonaGeografica;
	
	public Nacio(String codiNacio, String nom, ZonaGeografica zonaGeografica) throws Exception{
		setCodiNacio(codiNacio);
		setNom(nom);
		setZonaGeografica(zonaGeografica);
	}
	
	public String getCodiNacio() { return codiNacio;}
	public String getNom() { return nom;}
	public ZonaGeografica getZonaGeografica() { return zonaGeografica;}
	
	public void setCodiNacio(String codiNacio) throws Exception { 
		if (codiNacio.isEmpty()) throw new Exception("Codi naci� buit.");
		if (codiNacio.length() != 3) throw new Exception("Longitud codi naci� incorrecta.");
		this.codiNacio = codiNacio;
	}
	
	public void setNom(String nom) throws Exception { 
		if (nom.trim().isEmpty()) throw new Exception("El nom de naci� est� buit.");
		if (nom.length() > 40) throw new Exception("El nom de naci� es massa llarg.");
		this.nom = nom;
	}

	public void setZonaGeografica(ZonaGeografica zonaGeografica) throws Exception {
		if (zonaGeografica.getNomZona().trim().isEmpty()) throw new Exception("Zona geogr�fica buida.");
		if (zonaGeografica.getNomZona().length() > 40) throw new Exception("Zona geogr�fica massa llarga.");
		this.zonaGeografica = zonaGeografica;
	}

	public String toString(){
		return this.nom;
	}
}
