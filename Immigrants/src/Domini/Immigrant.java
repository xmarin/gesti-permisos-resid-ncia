package Domini;

public class Immigrant extends Persona{
	
	private String passaport;
	private Nacio nacio;
	
	public Immigrant(String passaport, Nacio nacio, String filiacio, String domicili, String poblacio) throws Exception {
		super(filiacio, domicili, poblacio);
		setPassaport(passaport);
		setNacio(nacio);
	}
	
	public String getPassaport() { return passaport;}
	public Nacio getNacio() { return nacio;}
	
	public void setPassaport(String passaport) throws Exception {
		if (passaport.trim().isEmpty()) throw new Exception("El n�mero de passaport est� buit.");
		if (passaport.length()!=12) throw new Exception("Longitud passaport incorrecta.");
		this.passaport = passaport;
	}

	public void setNacio(Nacio nacio) throws Exception {
		if (nacio==null) throw new Exception("Naci� no v�lida.");
		this.nacio = nacio;
	}

	public String toString(){
		return this.getFiliacio();
	}
}
