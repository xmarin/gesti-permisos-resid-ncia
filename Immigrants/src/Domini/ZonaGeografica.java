package Domini;

public class ZonaGeografica {
	
	private String nomZona;
	private Continent continent;
	
	public ZonaGeografica(String nomZona, Continent continent) throws Exception{
		setNomZona(nomZona);
		setContinent(continent);
	}

	public String getNomZona() { return nomZona;}
	public Continent getContinent() { return continent;}

	public void setNomZona(String nomZona) throws Exception {
		if (nomZona.trim().isEmpty()) throw new Exception("Nom zona geogr�fica no pot estar buit.");
		if (nomZona.length() > 40) throw new Exception("Nom zona geogr�fica massa gran.");
		this.nomZona = nomZona;
	}

	
	public void setContinent(Continent continent) throws Exception {
		if (continent == null) throw new Exception("Continent no v�lid.");
		if (continent.getNomContinent().length() > 40) throw new Exception("Nom continent massa llarg.");
		this.continent = continent;
	}

	
}
