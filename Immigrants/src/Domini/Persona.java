package Domini;

public abstract class Persona {
	
	private String filiacio, domicili, poblacio;

	public Persona (String Filiacio, String Domicili, String Poblacio) throws Exception{
		setFiliacio(Filiacio);
		setDomicili(Domicili);
		setPoblacio(Poblacio);
	}
	
	public String getFiliacio() { return filiacio;}
	public String getDomicili() { return domicili;}
	public String getPoblacio() { return poblacio;}
	
	public void setFiliacio(String filiacio) throws Exception{
		if (filiacio.length() > 40) throw new Exception("La mida del camp filiaci� es massa llarga");
		if(filiacio.trim().isEmpty()) throw new Exception("La filiaci� no pot ser buida.");
		
		this.filiacio = filiacio.toUpperCase();
	}
	
	public void setDomicili(String domicili) throws Exception{
		if (domicili.length() > 40) throw new Exception("La mida del camp domicili es massa llarga");
		if(domicili.trim().isEmpty()) throw new Exception("El domicili no pot ser buit.");
		
		this.domicili = domicili.toUpperCase();
	}
	
	public void setPoblacio(String poblacio) throws Exception{
		if (poblacio.length() > 40) throw new Exception("La mida del camp poblaci� es massa llarga");
		if(poblacio.trim().isEmpty()) throw new Exception("La poblaci� no pot ser buida.");
		
		this.poblacio = poblacio.toUpperCase();
	}
}
