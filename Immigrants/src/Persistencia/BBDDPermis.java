package Persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashSet;

import Domini.Amfitrio;
import Domini.Immigrant;
import Domini.PermisResidencia;

public class BBDDPermis {

	private ConnexioBBDD connexioBBDD;
	private BBDDAmfitrio connexioAmfitrio;
	private BBDDImmigrant connexioImmigrant;
	
	public BBDDPermis() throws Exception{
		connexioBBDD = ConnexioBBDD.getConnexio();
		connexioAmfitrio = new BBDDAmfitrio();
		connexioImmigrant = new BBDDImmigrant();
	}
	
	public int obtenirNumPermis() throws Exception{
		try{
			String sql= "SELECT SEQ_PERMISOS_NPERMIS.NEXTVAL FROM DUAL";
			PreparedStatement pst = this.connexioBBDD.prepareStatement(sql);
			ResultSet resultat = pst.executeQuery();
			if (resultat.next()){	
				return Integer.parseInt(resultat.getString(1));
			}
			return 0;
		}
		catch(Exception e){
			throw new Exception ("Error find: "+ e.getMessage());
		}	
	}
	
	public PermisResidencia obtenirUltimPermis(String cerca) throws Exception {
		
		if (cerca == null) throw new Exception ("El nif no es v�lid");
		try{
			
			String sql= "SELECT * FROM PERMIS WHERE NUM_PASSAPORT = ? OR NUM_PERMIS = ? ORDER BY DATA_CONCESSIO DESC";
			PreparedStatement pst = this.connexioBBDD.prepareStatement(sql);
			pst.setString(1, cerca);
			pst.setString(2, cerca);
			ResultSet resultat = pst.executeQuery();
			if (resultat.next()){	
				int numPermis = Integer.parseInt(resultat.getString("NUM_PERMIS"));
				
				String nif = resultat.getString("NIF");
				Amfitrio anf = connexioAmfitrio.find(nif);
				
				String pass = resultat.getString("NUM_PASSAPORT");
				Immigrant imm = connexioImmigrant.find(pass);
				
				String domicili = resultat.getString("DOMICILI");
				Date dataConcessio = resultat.getDate("DATA_CONCESSIO");
				String durada = resultat.getString("DURADA");
				String parentiu = resultat.getString("PARENTIU");
				
				PermisResidencia permis = new PermisResidencia(numPermis,anf,imm,dataConcessio,durada,domicili,parentiu);

				return permis;
			}
			
			return null;
		}
		catch(Exception e){
			throw new Exception ("Error find: "+ e.getMessage());
		}		
	}
	
	public void inserirPermis(PermisResidencia p) throws Exception{
		try{
			String sql = "INSERT INTO PERMIS (NUM_PERMIS, NIF, NUM_PASSAPORT, DATA_CONCESSIO, DURADA, DOMICILI, PARENTIU) VALUES (?,?,?,?,?,?,?)";
			PreparedStatement pst = this.connexioBBDD.prepareStatement(sql);
			pst.clearParameters();
			pst.setString(1, Integer.toString(p.getNumPermis()));
			pst.setString(2, p.getAmfitrio().getNif());
			pst.setString(3, p.getImmigrant().getPassaport());
			pst.setDate(4, new java.sql.Date(p.getDataConcessio().getTime()));
			pst.setString(5, Integer.toString(p.getDuradaConcessio()));
			pst.setString(6, p.getDomicili());
			pst.setString(7, p.getParentiu().getTipus());

			if (pst.executeUpdate()!=1){
				throw new Exception("Error al inserir perm�s de resid�ncia");
			}
		}catch(Exception e){
			throw new Exception("Error al inserir perm�s de resid�ncia: "+e.getMessage());
		}
	}
	
	public HashSet<PermisResidencia> ObtenirPermisos(String cerca) throws Exception{
		
		if (cerca == null) throw new Exception ("El passaport no �s v�lid");
		
		try{
			HashSet<PermisResidencia> resultats = new HashSet<PermisResidencia>();
			
			String sql= "SELECT * FROM PERMIS WHERE NUM_PASSAPORT = ? OR NUM_PERMIS = ?";
			PreparedStatement pst = this.connexioBBDD.prepareStatement(sql);
			pst.setString(1, cerca);
			pst.setString(2, cerca);
			ResultSet resultat = pst.executeQuery();
			while (resultat.next()){	
				int numPermis = Integer.parseInt(resultat.getString("NUM_PERMIS"));
				
				String nif = resultat.getString("NIF");
				Amfitrio anf = connexioAmfitrio.find(nif);
				
				String pass = resultat.getString("NUM_PASSAPORT");
				Immigrant imm = connexioImmigrant.find(pass);
				
				String domicili = resultat.getString("DOMICILI");
				Date dataConcessio = resultat.getDate("DATA_CONCESSIO");
				String durada = resultat.getString("DURADA");
				String parentiu = resultat.getString("PARENTIU");
				
				PermisResidencia permis = new PermisResidencia(numPermis,anf,imm,dataConcessio,durada,domicili,parentiu);
				
				resultats.add(permis);
			}
			if (resultats.isEmpty()) return null;
			return resultats;
		}
		catch(Exception e){
			throw new Exception ("Error find: "+ e.getMessage());
		}		
		
	}
}

