package Persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashSet;

import Domini.Amfitrio;
import Domini.Immigrant;
import Domini.PermisResidencia;

public class BBDDEstadistica {
	
	private ConnexioBBDD connexioBBDD;
	private BBDDAmfitrio connexioAmfitrio;
	private BBDDImmigrant connexioImmigrant;
	
	public BBDDEstadistica() throws Exception{
		connexioBBDD = ConnexioBBDD.getConnexio();
		connexioAmfitrio = new BBDDAmfitrio();
		connexioImmigrant = new BBDDImmigrant();
	}
	
	public HashSet<PermisResidencia> getPermisosAnyTancat(String any) throws Exception{
		if (any.trim().isEmpty()) throw new Exception ("L'any no pot estar buit.");
		try{
			HashSet<PermisResidencia> res = new HashSet<PermisResidencia>();
			String sql= "SELECT * FROM PERMIS WHERE DATA_CONCESSIO BETWEEN 01/01/? AND 31/12/?";
			PreparedStatement pst = this.connexioBBDD.prepareStatement(sql);
			pst.setString(1, any);
			pst.setString(2, any);
			ResultSet resultat = pst.executeQuery();
			while (resultat.next()){	
				int numPermis = Integer.parseInt(resultat.getString("NUM_PERMIS"));
				String nif = resultat.getString("NIF");
				Amfitrio anf = connexioAmfitrio.find(nif);
				String pass = resultat.getString("NUM_PASSAPORT");
				Immigrant imm = connexioImmigrant.find(pass);
				String domicili = resultat.getString("DOMICILI");
				Date dataConcessio = resultat.getDate("DATA_CONCESSIO");
				String durada = resultat.getString("DURADA");
				String parentiu = resultat.getString("PARENTIU");
				
				res.add(new PermisResidencia(numPermis,anf,imm,dataConcessio,durada,domicili,parentiu));
			}
			if (res.isEmpty()) return null;
			return res;
		}
		catch(Exception e){
			System.out.println(e);
			throw new Exception ("Error find: "+ e.getMessage());
		}	
	}
	
	

}
