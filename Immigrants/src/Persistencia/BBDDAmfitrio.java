package Persistencia;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import Domini.Amfitrio;
import Domini.RenovacioPermisResidencia;

public class BBDDAmfitrio {
	private ConnexioBBDD connexioBBDD;
	
	public BBDDAmfitrio() throws Exception{
		connexioBBDD = ConnexioBBDD.getConnexio();	
	}
	
	public Amfitrio find(String Nif) throws Exception {
		
		if (Nif == null) throw new Exception ("El nif no es v�lid");
		try{
			String sql= "SELECT * FROM AMFITRIO WHERE NIF = ?";
			PreparedStatement pst = this.connexioBBDD.prepareStatement(sql);
			pst.setString(1, Nif);
			ResultSet resultat = pst.executeQuery();
			if (resultat.next()){	
				String nif = resultat.getString("NIF");
				String domicili = resultat.getString("Domicili");
				String poblacio = resultat.getString("Poblacio");
				String filiacio = resultat.getString("Filiacio");
				return new Amfitrio(nif,domicili,filiacio,poblacio);	
			}
			return null;
		}
		catch(Exception e){
			System.out.println(e);
			throw new Exception ("Error find: "+ e.getMessage());
		}		
	}
	
	public void inserirAmfitrio(Amfitrio amf) throws Exception{
		try{
			String sql = "INSERT INTO AMFITRIO (NIF,DOMICILI,POBLACIO,FILIACIO) VALUES (?,?,?,?)";
			PreparedStatement pst = this.connexioBBDD.prepareStatement(sql);
			pst.clearParameters();
			pst.setString(1, amf.getNif());
			pst.setString(2, amf.getDomicili());
			pst.setString(3, amf.getPoblacio());
			pst.setString(4, amf.getFiliacio());
						
			if (pst.executeUpdate()!=1){
				throw new Exception("Error al inserir amfitri�");
			}
		}catch(Exception e){
			throw new Exception("Error al inserir amfitri�: "+e.getMessage());
		}
		
	}
	
	public void modificarAmfitrio(Amfitrio amf) throws Exception{
		try{
			String sql = "UPDATE AMFITRIO SET DOMICILI = ?, POBLACIO = ?, FILIACIO = ? WHERE NIF = ?";
			PreparedStatement pst = this.connexioBBDD.prepareStatement(sql);
			pst.clearParameters();
	
			pst.setString(1, amf.getDomicili());
			pst.setString(2, amf.getPoblacio());
			pst.setString(3, amf.getFiliacio());
			pst.setString(4, amf.getNif());
			
				
			if (pst.executeUpdate()!=1){
				throw new Exception("Error al modificar amfitri�");
			}
		}catch(Exception e){
			throw new Exception("Error al modificar amfitri�: " + e.getMessage());
		}
		
	}

	public HashMap<String, Amfitrio> searchAmfitrio(String nifOrName) throws Exception {
		HashMap<String, Amfitrio> res = new HashMap<String,Amfitrio>();

		try{
			String sql = "SELECT * FROM AMFITRIO WHERE FILIACIO like ? or NIF = ? ORDER BY FILIACIO ASC";
			PreparedStatement pst = this.connexioBBDD.prepareStatement(sql);
			pst.setString(1, "%"+nifOrName+"%");
			pst.setString(2, nifOrName);
			ResultSet resultat = pst.executeQuery();
			
			while (resultat.next()){
				String NIF = resultat.getString("NIF");
				String nom = resultat.getString("FILIACIO");
				String domicili = resultat.getString("DOMICILI");
				String poblacio = resultat.getString("POBLACIO");
				res.put(NIF,new Amfitrio(NIF,domicili,nom,poblacio));
			}
			
			return res;
		}
		catch(Exception e){
			throw new Exception ("Error find: "+ e.getMessage());
		}
	}
	
	public HashMap<String, String[]> getHistorialAmfitrio(String nif) throws Exception {
		HashMap<String, String[]> res = new HashMap<String,String[]>();
		
		try{
			String sql = "SELECT PERMIS.NUM_PERMIS, IMMIGRANT.FILIACIO, NACIO.NOM_NACIO, PERMIS.PARENTIU, "
					+ "PERMIS.DATA_CONCESSIO, PERMIS.DURADA, a.DATA_RENOVACIO, a.DURADA_RENOVACIO "
					+ "FROM PERMIS "
					+ "INNER JOIN IMMIGRANT on PERMIS.NUM_PASSAPORT = IMMIGRANT.NUM_PASSAPORT "
					+ "INNER JOIN NACIO on IMMIGRANT.CODI_NACIO = NACIO.CODI_NACIO "
					+ "LEFT JOIN ( "
					+ "            SELECT NUM_PERMIS, DATA_RENOVACIO, DURADA_RENOVACIO "
					+ "            FROM (SELECT * from RENOVACIO order by DATA_RENOVACIO desc) "
					+ "            WHERE ROWNUM = 1 "
					+ "          ) a ON PERMIS.NUM_PERMIS = a.NUM_PERMIS "
					+ "WHERE PERMIS.NIF = ? "
					+ "ORDER BY PERMIS.DATA_CONCESSIO asc";
			
			PreparedStatement pst = this.connexioBBDD.prepareStatement(sql);
			pst.setString(1, nif);
			ResultSet resultat = pst.executeQuery();
			
			while (resultat.next()){
				Calendar c2 = new GregorianCalendar();
				SimpleDateFormat sdf2 = new SimpleDateFormat("yy-MM-dd hh:mm:ss");
				String[] data = new String[6];
				data[0] = resultat.getString("NUM_PERMIS");
				data[1] = resultat.getString("FILIACIO");
				data[2] = resultat.getString("NOM_NACIO");
				data[3] = resultat.getString("PARENTIU");
				
				c2.setTime(sdf2.parse(resultat.getString("DATA_CONCESSIO")));
				sdf2.applyPattern("dd/MM/yyyy");
				data[4] = sdf2.format(c2.getTime());
				
				Calendar c1 = new GregorianCalendar();
				SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd hh:mm:ss");
				
				if (resultat.getString("DATA_RENOVACIO") != null){
					c1.setTime(sdf.parse(resultat.getString("DATA_RENOVACIO")));
					c1.add(Calendar.MONTH, Integer.parseInt(resultat.getString("DURADA_RENOVACIO")));
					sdf.applyPattern("dd/MM/yyyy");
					data[5] = sdf.format(c1.getTime());
				}else{
					c1.setTime(sdf.parse(resultat.getString("DATA_CONCESSIO")));
					c1.add(Calendar.MONTH, Integer.parseInt(resultat.getString("DURADA")));
					sdf.applyPattern("dd/MM/yyyy");
					data[5] = sdf.format(c1.getTime());
				}
				res.put(data[0],data);
			}
			return res;
		}
		catch(Exception e){
			throw new Exception ("Error find: "+ e.getMessage());
		}
	}	
}
