package Persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashSet;

import Domini.RenovacioPermisResidencia;

public class BBDDRenovacio {
	
	private ConnexioBBDD connexioBBDD;
	
	public BBDDRenovacio() throws Exception{
		connexioBBDD = ConnexioBBDD.getConnexio();
	}
	
	public int obtenirNumPermis() throws Exception{
		try{
			String sql= "SELECT SEQ_PERMISOS_NPERMIS.NEXTVAL FROM DUAL";
			PreparedStatement pst = this.connexioBBDD.prepareStatement(sql);
			ResultSet resultat = pst.executeQuery();
			if (resultat.next()){	
				return Integer.parseInt(resultat.getString(1));
			}
			return 0;
		}
		catch(Exception e){
			throw new Exception ("Error find: "+ e.getMessage());
		}	
	}
	
	public HashSet<RenovacioPermisResidencia> obtenirRenovacions(String permis) throws Exception {
		
		if (permis == null) throw new Exception ("El permis no es v�lid");
		
		try{
			HashSet<RenovacioPermisResidencia> res = new HashSet<RenovacioPermisResidencia>();
			String sql = "SELECT * FROM RENOVACIO WHERE NUM_PERMIS = ? ORDER BY DATA_RENOVACIO DESC";
			PreparedStatement pst = this.connexioBBDD.prepareStatement(sql);
			pst.setString(1, permis);
			ResultSet resultat = pst.executeQuery();
			
			while (resultat.next()){	
				int numPermis = Integer.parseInt(resultat.getString("NUM_PERMIS"));
				Date dataConcessio = resultat.getDate("DATA_RENOVACIO");
				String durada = resultat.getString("DURADA_RENOVACIO");
				String domicili = resultat.getString("DOMICILI");

				res.add(new RenovacioPermisResidencia(numPermis,dataConcessio,durada,domicili));
			}
			
			if (res.isEmpty()) return null;
			return res;
			
		} catch(Exception e){
			throw new Exception ("Error find: "+ e.getMessage());
		}		
	}
	
	public RenovacioPermisResidencia obtenirUltimaRenovacio(String permis) throws Exception {
		
		if (permis == null) throw new Exception ("El permis no es v�lid");
		
		try{
			
			String sql = "SELECT * FROM RENOVACIO WHERE NUM_PERMIS = ? ORDER BY DATA_RENOVACIO DESC";
			PreparedStatement pst = this.connexioBBDD.prepareStatement(sql);
			pst.setString(1, permis);
			ResultSet resultat = pst.executeQuery();
			
			if (resultat.next()){	
				int numPermis = Integer.parseInt(resultat.getString("NUM_PERMIS"));
				Date dataConcessio = resultat.getDate("DATA_RENOVACIO");
				String durada = resultat.getString("DURADA_RENOVACIO");
				String domicili = resultat.getString("DOMICILI");

				return new RenovacioPermisResidencia(numPermis,dataConcessio,durada,domicili);
			}
			
			return null;
			
		} catch(Exception e){
			throw new Exception ("Error find: "+ e.getMessage());
		}		
	}
	
	public void inserirRenovacio(RenovacioPermisResidencia r) throws Exception{
		try{
			String sql = "INSERT INTO RENOVACIO (NUM_PERMIS, DATA_RENOVACIO, DURADA_RENOVACIO, DOMICILI) VALUES (?,?,?,?)";
			PreparedStatement pst = this.connexioBBDD.prepareStatement(sql);
			pst.clearParameters();
			pst.setString(1, Integer.toString(r.getNumPermis()));
			pst.setDate(2, new java.sql.Date(r.getDataRenovacio().getTime()));
			pst.setString(3, Integer.toString(r.getDuradaRenovacio()));
			pst.setString(4, r.getDomicili());
			
			if (pst.executeUpdate()!=1){
				throw new Exception("Error al inserir permis de resid�ncia");
			}
		}catch(Exception e){
			throw new Exception("Error al inserir perm�s de resid�ncia: "+e.getMessage());
		}
	}
}
