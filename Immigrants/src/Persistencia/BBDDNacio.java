package Persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;

import Domini.Continent;
import Domini.Nacio;
import Domini.ZonaGeografica;

public class BBDDNacio {
	
	private ConnexioBBDD connexio;
	
	public BBDDNacio() throws Exception{
		connexio = ConnexioBBDD.getConnexio();
	}
	
	public HashMap<String,Nacio> findNacio() throws Exception {
		
		try{
			
			HashMap<String,Nacio> res = new HashMap<String,Nacio>();
			String sql = "SELECT CODI_NACIO, NOM_ZONA_GEOGRAFICA, NOM_NACIO, CONTINENT.NOM_CONTINENT FROM NACIO "
					+ "INNER JOIN CONTINENT "
					+ "ON NACIO.CONTINENT ="
					+ "CONTINENT.NOM_CONTINENT";
			PreparedStatement pst = this.connexio.prepareStatement(sql);
			ResultSet resultat = pst.executeQuery();
			
			while (resultat.next()){
				String codi = resultat.getString("CODI_NACIO");
				String nom = resultat.getString("NOM_NACIO");
				ZonaGeografica zona = new ZonaGeografica(resultat.getString("NOM_ZONA_GEOGRAFICA"), new Continent(resultat.getString("NOM_CONTINENT")));
				
				res.put(codi,new Nacio(codi,nom,zona));
			}
			
			if (res.isEmpty()) return null;
			return res;
		}
		catch(Exception e){
			throw new Exception ("Error find: "+ e.getMessage());
		}		
	}

}
