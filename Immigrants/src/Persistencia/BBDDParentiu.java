package Persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashSet;


public class BBDDParentiu {
	
	private ConnexioBBDD connexio;
	
	public BBDDParentiu() throws Exception{
		connexio = ConnexioBBDD.getConnexio();
	}
	
	public HashSet<String> findParentius() throws Exception {
		
		try{
			HashSet<String> res = new HashSet<String>();
			String sql = "SELECT * FROM PARENTIU";
			PreparedStatement pst = this.connexio.prepareStatement(sql);
			ResultSet resultat = pst.executeQuery();
			
			while (resultat.next()){
				String tipusParentiu = resultat.getString("TIPUS_PARENTIU");
				res.add(tipusParentiu);
			}

			if (res.isEmpty()) return null;
			return res;
		}
		catch(Exception e){
			throw new Exception ("Error find: "+ e.getMessage());
		}		
	}
}
