package Persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BBDDFuncionari {
	private ConnexioBBDD connexioBBDD;
	
	public BBDDFuncionari(ConnexioBBDD con){
		this.connexioBBDD = con;
	}
	
	public boolean login(String user, String pwd) throws Exception{
		try{
			String sql = "select * from FUNCIONARI where USUARI = ? and CONTRASENYA = ?";
			PreparedStatement pst = this.connexioBBDD.prepareStatement(sql);
			pst.setString(1, user);
			pst.setString(2, pwd);
			ResultSet resultat = pst.executeQuery();
			
			return resultat.next();
		}catch (Exception e){
			throw new Exception("Error: " + e.getMessage());
		}
	}
	
	public void close() throws SQLException{
		connexioBBDD.close();
	}
}