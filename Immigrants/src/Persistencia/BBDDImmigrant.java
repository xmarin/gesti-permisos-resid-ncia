package Persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;

import Domini.Immigrant;
import Domini.Nacio;
import Domini.RenovacioPermisResidencia;

public class BBDDImmigrant {

	
private ConnexioBBDD connexioBBDD;
	private BBDDNacio connexioNacio;
	private BBDDRenovacio renovacioBD;
	
	public BBDDImmigrant() throws Exception{
		this.connexioBBDD = ConnexioBBDD.getConnexio();
		this.connexioNacio = new BBDDNacio();
		this.renovacioBD = new BBDDRenovacio();
	}
	
	public Immigrant find(String passaport) throws Exception {
		if (passaport == null) throw new Exception ("Passaport no v�lid");
		try{
			String sql = "select * from IMMIGRANT where NUM_PASSAPORT = ?";
			PreparedStatement pst = this.connexioBBDD.prepareStatement(sql);
			pst.setString(1, passaport);
			ResultSet resultat = pst.executeQuery();
			
			if (resultat.next()){	
				String nPassaport = resultat.getString(1);
				Nacio nacio = connexioNacio.findNacio().get(resultat.getString(2));
				String domicili = resultat.getString(3);
				String poblacio = resultat.getString(4);
				String filiacio = resultat.getString(5);
				return new Immigrant(nPassaport,nacio,filiacio,domicili,poblacio);
			}
			return null;
		}
		catch(Exception e){
			throw new Exception ("Error find: "+ e.getMessage());
		}		
	}
	
	public void inserirImmigrant(Immigrant immi) throws Exception{
		try{
			String sql = "INSERT INTO IMMIGRANT (NUM_PASSAPORT,CODI_NACIO,DOMICILI,POBLACIO,FILIACIO) VALUES (?,?,?,?,?)";
			PreparedStatement pst = this.connexioBBDD.prepareStatement(sql);
			pst.clearParameters();
			pst.setString(1, immi.getPassaport());
			pst.setString(2, immi.getNacio().getCodiNacio());
			pst.setString(3, immi.getDomicili());
			pst.setString(4, immi.getPoblacio());
			pst.setString(5, immi.getFiliacio());
						
			if (pst.executeUpdate()!=1){
				throw new Exception("Error al inserir immigrant");
			}
		}catch(Exception e){
			throw new Exception("Error al inserir immigrant: "+e.getMessage());
		}
		
	}
	
	public void modificarImmigrant(Immigrant immi) throws Exception{
		try{
			String sql = "UPDATE IMMIGRANT SET CODI_NACIO = ? ,DOMICILI = ?, POBLACIO = ?, FILIACIO = ? WHERE NUM_PASSAPORT = ?";
			PreparedStatement pst = this.connexioBBDD.prepareStatement(sql);
			pst.clearParameters();
			
			pst.setString(1, immi.getNacio().getCodiNacio());
			pst.setString(2, immi.getDomicili());
			pst.setString(3, immi.getPoblacio());
			pst.setString(4, immi.getFiliacio());
			pst.setString(5, immi.getPassaport());
						
			if (pst.executeUpdate()!=1){
				throw new Exception("Error al modificar immigrant");
			}
		}catch(Exception e){
			throw new Exception("Error al modificar immigrant: " + e.getMessage());
		}
		
	}

	public Integer getNumImmigrants() throws Exception {
		try{
			String sql = "SELECT COUNT(*) FROM IMMIGRANT";
			PreparedStatement pst = this.connexioBBDD.prepareStatement(sql);
			
			ResultSet resultat = pst.executeQuery();
			
			if (resultat.next()){
				return resultat.getInt(1);
			}
			return 0;
		}catch (Exception e){
			throw new Exception("Error: " + e.getMessage());
		}
	}
	
	public HashMap<String,Immigrant> searchImmigrants(String nameOrPassport) throws Exception{
		HashMap<String,Immigrant> res = new HashMap<String,Immigrant>();

		try{
			String sql = "SELECT * FROM IMMIGRANT WHERE FILIACIO LIKE ? OR NUM_PASSAPORT = ? order by FILIACIO asc";
			PreparedStatement pst = this.connexioBBDD.prepareStatement(sql);
			pst.setString(1, "%" + nameOrPassport + "%");
			pst.setString(2, nameOrPassport);
			
			ResultSet resultat = pst.executeQuery();
			
			while (resultat.next()){
				String nPassaport = resultat.getString(1);
				Nacio nacio = connexioNacio.findNacio().get(resultat.getString(2));
				String domicili = resultat.getString(3);
				String poblacio = resultat.getString(4);
				String filiacio = resultat.getString(5);
				res.put(nPassaport,new Immigrant(nPassaport,nacio,filiacio,domicili,poblacio));
			}
			return res;
		}catch (Exception e){
			throw new Exception("Error: " + e.getMessage());
		}
	}

	public String getHistorialImmigrant(String passaport) throws Exception {
		String res = "";
		
		try{
			String sql = "SELECT PERMIS.NUM_PERMIS, AMFITRIO.FILIACIO, PERMIS.PARENTIU, PERMIS.DATA_CONCESSIO AS DATA_CONCESSIO, PERMIS.DURADA AS DURADA, PERMIS.DOMICILI "
					+ "FROM PERMIS "
					+ "INNER JOIN AMFITRIO ON PERMIS.NIF = AMFITRIO.NIF "
					+ "WHERE NUM_PASSAPORT = ? ";
			
			PreparedStatement pst = this.connexioBBDD.prepareStatement(sql);
			pst.setString(1, passaport);
			ResultSet resultat = pst.executeQuery();
			
			

			while (resultat.next()){
				int nd=1;
				Calendar c2 = new GregorianCalendar();
				SimpleDateFormat sdf2 = new SimpleDateFormat("yy-MM-dd hh:mm:ss");
				
				res += "\t N�mero de perm�s: \t" + resultat.getString("NUM_PERMIS");
				res += "\n\t Amfitri�: \t\t" + resultat.getString("FILIACIO");
				res += "\n\t Parentiu: \t\t" + resultat.getString("PARENTIU");
				
				c2.setTime(sdf2.parse(resultat.getString("DATA_CONCESSIO")));
				sdf2.applyPattern("dd/MM/yyyy");
				
				res += "\n\t Data Concessi�: \t" + sdf2.format(c2.getTime());
				
				
				RenovacioPermisResidencia rp = renovacioBD.obtenirUltimaRenovacio(resultat.getString("NUM_PERMIS"));
				
				if (rp != null){
					c2.setTime(rp.getDataRenovacio());
					c2.add(Calendar.MONTH, Integer.parseInt(resultat.getString("DURADA")));
					res += "\n\t Data Retorn: \t\t" + sdf2.format(c2.getTime());
				}else{
					sdf2.applyPattern("yy-MM-dd hh:mm:ss");
					c2.setTime(sdf2.parse(resultat.getString("DATA_CONCESSIO")));
					c2.add(Calendar.MONTH, Integer.parseInt(resultat.getString("DURADA")));
					sdf2.applyPattern("dd/MM/yyyy");
					res += "\n\t Data Retorn: \t\t" + sdf2.format(c2.getTime());
				}
				
				res += "\n\t Domicilis: \t\t" + nd+". "+resultat.getString("DOMICILI");
				
				HashSet<RenovacioPermisResidencia> renovacions = renovacioBD.obtenirRenovacions(resultat.getString("NUM_PERMIS"));
				
				if (renovacions != null){
					for (RenovacioPermisResidencia r : renovacions){
						nd++;
						res += "\n\t\t\t" + nd+". "+ r.getDomicili();
					}
				}
				res += "\n\n";
			}
			return res;
		}
		catch(Exception e){
			throw new Exception ("Error find: "+ e.getMessage());
		}
	}
}
