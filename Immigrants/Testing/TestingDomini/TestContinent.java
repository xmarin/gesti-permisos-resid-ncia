package TestingDomini;

import static org.junit.Assert.*;

import org.junit.Test;

import Domini.Continent;

public class TestContinent {

	private Continent con;

	@Test
	public void testNomNull() {
		// Comprovem que el nom del continent es null
		try {
			con = new Continent(null);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testgetNomContinent() throws Exception {
		// Comprovem que el get recupera el valor correctament.
		con = new Continent("Pangea");
		assertEquals("Pangea", con.getNomContinent());
	}

	@Test
	public void testsetNomContinent() throws Exception {
		// Comprovem que el set canvia el valor correctament.

		con = new Continent("Pang");
		con.setNomContinent("Pangea");
		assertEquals("Pangea", con.getNomContinent());
	}

}
