package TestingDomini;

import static org.junit.Assert.*;

import org.junit.Test;

import Domini.Amfitrio;
import Domini.Continent;
import Domini.Immigrant;
import Domini.Nacio;
import Domini.ZonaGeografica;

public class TestImmigrant {

	private Immigrant immi;
	private Continent cont;
	private ZonaGeografica zg;
	private Nacio nac;

	@Test
	public void testPassaportNull() {
		// Comprovem que el passaport es null

		// es crea una nacio correcta
		try {
			cont = new Continent("Europa");
			zg = new ZonaGeografica("Europa Oriental", cont);
			nac = new Nacio("ESP", "ESPANYA", zg);
			immi = new Immigrant(null, nac, "JUAN PITO", "C/PLAYA", "BADALONA");
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testPassaportMidaIncorrecte() {
		// Comprovem que el passaport es de mida incorrecte

		// es crea una nacio correcta
		try {
			cont = new Continent("Europa");
			zg = new ZonaGeografica("Europa Oriental", cont);
			nac = new Nacio("ESP", "ESPANYA", zg);
			immi = new Immigrant("0147", nac, "JUAN PITO", "C/PLAYA",
					"BADALONA");
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testFiliacioNull() {
		// Comprovem que la filiacio es null

		// es crea una nacio correcta
		try {
			cont = new Continent("Europa");
			zg = new ZonaGeografica("Europa Oriental", cont);
			nac = new Nacio("ESP", "ESPANYA", zg);
			immi = new Immigrant("123456789123", nac, null, "C/PLAYA",
					"BADALONA");
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testFiliacioLlargaria() {
		// Comprovem que la filiacio sobrepassa la llargaria max de la BD.

		// es crea una nacio correcta
		try {
			cont = new Continent("Europa");
			zg = new ZonaGeografica("Europa Oriental", cont);
			nac = new Nacio("ESP", "ESPANYA", zg);
			immi = new Immigrant("123456789123", nac,
					"AAAAAAAAAABBBBBBBBBBCCCCCCCCCCDDDDDDDDDDE", "C/PLAYA",
					"BADALONA");
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testDomiciliNull() {
		// Comprovem que el domicili es null

		// es crea una nacio correcta
		try {
			cont = new Continent("Europa");
			zg = new ZonaGeografica("Europa Oriental", cont);
			nac = new Nacio("ESP", "ESPANYA", zg);
			immi = new Immigrant("123456789123", nac, "JUAN PITO", null,
					"BADALONA");
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testDomiciliLlargaria() {
		// Comprovem que el domicili sobrepassa la llargaria max de la BD.

		// es crea una nacio correcta
		try {
			cont = new Continent("Europa");
			zg = new ZonaGeografica("Europa Oriental", cont);
			nac = new Nacio("ESP", "ESPANYA", zg);
			immi = new Immigrant("123456789123", nac, "JUAN PITO",
					"AAAAAAAAAABBBBBBBBBBCCCCCCCCCCDDDDDDDDDDE", "BADALONA");
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testPoblacioNull() {
		// Comprovem que la poblacio es null

		// es crea una nacio correcta
		try {
			cont = new Continent("Europa");
			zg = new ZonaGeografica("Europa Oriental", cont);
			nac = new Nacio("ESP", "ESPANYA", zg);
			immi = new Immigrant("123456789123", nac, "JUAN PITO", "C/PLAYA",
					null);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testPoblacioLlargaria() {
		// Comprovem que la poblacio sobrepassa la llargaria max de la BD.

		// es crea una nacio correcta
		try {
			cont = new Continent("Europa");
			zg = new ZonaGeografica("Europa Oriental", cont);
			nac = new Nacio("ESP", "ESPANYA", zg);
			immi = new Immigrant("123456789123", nac, "JUAN PITO", "C/PLAYA",
					"AAAAAAAAAABBBBBBBBBBCCCCCCCCCCDDDDDDDDDDE");
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testgetPassaport() throws Exception {
		// Comprovem que el get recupera el valor correctament.
		cont = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", cont);
		nac = new Nacio("ESP", "ESPANYA", zg);
		immi = new Immigrant("123456789123", nac, "JUAN PITO", "C/PLAYA",
				"BADALONA");

		assertEquals("123456789123", immi.getPassaport());
	}

	@Test
	public void testgetNacio() throws Exception {
		// Comprovem que el get recupera el valor correctament.

		cont = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", cont);
		nac = new Nacio("ESP", "ESPANYA", zg);
		immi = new Immigrant("123456789123", nac, "JUAN PITO", "C/PLAYA",
				"BADALONA");

		assertEquals(nac, immi.getNacio());
	}

	@Test
	public void testsetPassaport() throws Exception {
		// Comprovem que el set canvia el valor correctament.

		cont = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", cont);
		nac = new Nacio("ESP", "ESPANYA", zg);
		immi = new Immigrant("123456789123", nac, "JUAN PITO", "C/PLAYA",
				"BADALONA");

		immi.setPassaport("000000000000");

		assertEquals("000000000000", immi.getPassaport());
	}

	@Test
	public void testsetNacio() throws Exception {
		// Comprovem que el set canvia el valor correctament.

		cont = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", cont);
		nac = new Nacio("ESP", "ESPANYA", zg);
		immi = new Immigrant("123456789123", nac, "JUAN PITO", "C/PLAYA",
				"BADALONA");
		nac = new Nacio("AND", "ANDORRA", zg);

		immi.setNacio(nac);
		assertEquals(nac, immi.getNacio());
	}

}
