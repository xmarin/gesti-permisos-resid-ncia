package TestingDomini;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

import Domini.Amfitrio;
import Domini.Continent;
import Domini.Immigrant;
import Domini.Nacio;
import Domini.PermisResidencia;
import Domini.RenovacioPermisResidencia;
import Domini.ZonaGeografica;

public class TestPermisResidencia {

	private PermisResidencia pr;
	private Amfitrio amf, amf2;
	private Immigrant imm;
	private Nacio nac;
	private ZonaGeografica zg;
	private Continent con;
	private Date data = new Date();

	@Test
	public void testNumPermisNumeroPositiu() {
		// Comprovem que el num del permis es < 0

		try {
			con = new Continent("Europa");
			zg = new ZonaGeografica("Europa Oriental", con);
			nac = new Nacio("ESP", "Espanya", zg);
			amf = new Amfitrio("00000000T", "C/PLAYA", "JUAN MAGAN", "BADALONA");
			imm = new Immigrant("123456789123", nac, "MOHA", "C/TANGER",
					"BADALONA");

			pr = new PermisResidencia(-9, amf, imm, data, "12", "C/PLAYA",
					"AVI");
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testAmfitrionull() {
		// Comprovem que l'amfitrio es null

		try {
			con = new Continent("Europa");
			zg = new ZonaGeografica("Europa Oriental", con);
			nac = new Nacio("ESP", "Espanya", zg);
			imm = new Immigrant("123456789123", nac, "MOHA", "C/TANGER",
					"BADALONA");

			pr = new PermisResidencia(25, null, imm, data, "12", "C/PLAYA",
					"AVI");
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testImmigrantnull() {
		// Comprovem que el immigrant es null

		try {
			con = new Continent("Europa");
			zg = new ZonaGeografica("Europa Oriental", con);
			nac = new Nacio("ESP", "Espanya", zg);
			amf = new Amfitrio("00000000T", "C/PLAYA", "JUAN MAGAN", "BADALONA");

			pr = new PermisResidencia(25, amf, null, data, "12", "C/PLAYA",
					"AVI");
			fail();
		} catch (Exception e) {
			assert (true);
		}

	}

	@Test
	public void dataNull() {
		// comprovem que la data es null
		try {
			con = new Continent("Europa");
			zg = new ZonaGeografica("Europa Oriental", con);
			nac = new Nacio("ESP", "Espanya", zg);
			amf = new Amfitrio("00000000T", "C/PLAYA", "JUAN MAGAN", "BADALONA");
			imm = new Immigrant("123456789123", nac, "MOHA", "C/TANGER",
					"BADALONA");

			pr = new PermisResidencia(25, amf, imm, null, "12", "C/PLAYA",
					"AVI");
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testDuradaConcessioNull() {
		// Comprovem que la durada de la concessio es null

		try {
			con = new Continent("Europa");
			zg = new ZonaGeografica("Europa Oriental", con);
			nac = new Nacio("ESP", "Espanya", zg);
			amf = new Amfitrio("00000000T", "C/PLAYA", "JUAN MAGAN", "BADALONA");
			imm = new Immigrant("123456789123", nac, "MOHA", "C/TANGER",
					"BADALONA");

			pr = new PermisResidencia(25, amf, imm, data, null, "C/PLAYA",
					"AVI");
			fail();
		} catch (Exception e) {
			assert (true);
		}

	}

	@Test
	public void testDuradaConcessioLletra() {
		// Comprovem que la durada de la concessio es una lletra

		try {
			con = new Continent("Europa");
			zg = new ZonaGeografica("Europa Oriental", con);
			nac = new Nacio("ESP", "Espanya", zg);
			amf = new Amfitrio("00000000T", "C/PLAYA", "JUAN MAGAN", "BADALONA");
			imm = new Immigrant("123456789123", nac, "MOHA", "C/TANGER",
					"BADALONA");

			pr = new PermisResidencia(25, amf, imm, data, "a", "C/PLAYA", "AVI");
			fail();
		} catch (Exception e) {
			assert (true);
		}

	}

	@Test
	public void testDomiciliNull() {
		// Comprovem que el domicili sigui null

		try {
			con = new Continent("Europa");
			zg = new ZonaGeografica("Europa Oriental", con);
			nac = new Nacio("ESP", "Espanya", zg);
			amf = new Amfitrio("00000000T", "C/PLAYA", "JUAN MAGAN", "BADALONA");
			imm = new Immigrant("123456789123", nac, "MOHA", "C/TANGER",
					"BADALONA");

			pr = new PermisResidencia(25, amf, imm, data, "23", null, "AVI");
			fail();
		} catch (Exception e) {
			assert (true);
		}

	}

	@Test
	public void testDomiciliLlargaria() {
		// Comprovem que el domicili sobrepassa la llargaria max de la BD.

		try {
			con = new Continent("Europa");
			zg = new ZonaGeografica("Europa Oriental", con);
			nac = new Nacio("ESP", "Espanya", zg);
			amf = new Amfitrio("00000000T", "C/PLAYA", "JUAN MAGAN", "BADALONA");
			imm = new Immigrant("123456789123", nac, "MOHA", "C/TANGER",
					"BADALONA");

			pr = new PermisResidencia(25, amf, imm, data, "23",
					"AAAAAAAAAABBBBBBBBBBCCCCCCCCCCDDDDDDDDDDE", "AVI");
			fail();
		} catch (Exception e) {
			assert (true);
		}

	}

	@Test
	public void testParentiuNull() {
		// Comprovem que el domicili sobrepassa la llargaria max de la BD.

		try {
			con = new Continent("Europa");
			zg = new ZonaGeografica("Europa Oriental", con);
			nac = new Nacio("ESP", "Espanya", zg);
			amf = new Amfitrio("00000000T", "C/PLAYA", "JUAN MAGAN", "BADALONA");
			imm = new Immigrant("123456789123", nac, "MOHA", "C/TANGER",
					"BADALONA");

			pr = new PermisResidencia(25, amf, imm, data, "23", "C/PLAYA", null);
			fail();
		} catch (Exception e) {
			assert (true);
		}

	}

	@Test
	public void testParentiuLlargaria() {
		// Comprovem que el domicili sobrepassa la llargaria max de la BD.

		try {
			con = new Continent("Europa");
			zg = new ZonaGeografica("Europa Oriental", con);
			nac = new Nacio("ESP", "Espanya", zg);
			amf = new Amfitrio("00000000T", "C/PLAYA", "JUAN MAGAN", "BADALONA");
			imm = new Immigrant("123456789123", nac, "MOHA", "C/TANGER",
					"BADALONA");

			pr = new PermisResidencia(25, amf, imm, data, "23", "C/PLAYA",
					"AVIIIIIIIII");
			fail();
		} catch (Exception e) {
			assert (true);
		}

	}

	@Test
	public void testgetNumPermis() throws Exception {
		// Comprovem que el get recupera el valor correctament.

		con = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", con);
		nac = new Nacio("ESP", "Espanya", zg);
		amf = new Amfitrio("00000000T", "C/PLAYA", "JUAN MAGAN", "BADALONA");
		imm = new Immigrant("123456789123", nac, "MOHA", "C/TANGER", "BADALONA");
		pr = new PermisResidencia(25, amf, imm, data, "23", "C/PLAYA", "AVI");

		assertEquals(25, pr.getNumPermis());
	}

	@Test
	public void testgetDataConcessio() throws Exception {
		// Comprovem que el get recupera el valor correctament.

		con = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", con);
		nac = new Nacio("ESP", "Espanya", zg);
		amf = new Amfitrio("00000000T", "C/PLAYA", "JUAN MAGAN", "BADALONA");
		imm = new Immigrant("123456789123", nac, "MOHA", "C/TANGER", "BADALONA");
		pr = new PermisResidencia(25, amf, imm, data, "23", "C/PLAYA", "AVI");

		assertEquals(data, pr.getDataConcessio());
	}

	@Test
	public void testgetDuradaConcessio() throws Exception {
		// Comprovem que el get recupera el valor correctament.

		con = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", con);
		nac = new Nacio("ESP", "Espanya", zg);
		amf = new Amfitrio("00000000T", "C/PLAYA", "JUAN MAGAN", "BADALONA");
		imm = new Immigrant("123456789123", nac, "MOHA", "C/TANGER", "BADALONA");
		pr = new PermisResidencia(25, amf, imm, data, "23", "C/PLAYA", "AVI");

		assertEquals(23, pr.getDuradaConcessio());
	}

	@Test
	public void testgetAmfitrio() throws Exception {
		// Comprovem que el get recupera el valor correctament.

		con = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", con);
		nac = new Nacio("ESP", "Espanya", zg);
		amf = new Amfitrio("00000000T", "C/PLAYA", "JUAN MAGAN", "BADALONA");
		imm = new Immigrant("123456789123", nac, "MOHA", "C/TANGER", "BADALONA");
		pr = new PermisResidencia(25, amf, imm, data, "23", "C/PLAYA", "AVI");

		assertEquals(amf, pr.getAmfitrio());
	}

	@Test
	public void testgetImmigrant() throws Exception {
		// Comprovem que el get recupera el valor correctament.

		con = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", con);
		nac = new Nacio("ESP", "Espanya", zg);
		amf = new Amfitrio("00000000T", "C/PLAYA", "JUAN MAGAN", "BADALONA");
		imm = new Immigrant("123456789123", nac, "MOHA", "C/TANGER", "BADALONA");
		pr = new PermisResidencia(25, amf, imm, data, "23", "C/PLAYA", "AVI");

		assertEquals(imm, pr.getImmigrant());
	}

	@Test
	public void testgetDomicili() throws Exception {
		// Comprovem que el get recupera el valor correctament.

		con = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", con);
		nac = new Nacio("ESP", "Espanya", zg);
		amf = new Amfitrio("00000000T", "C/PLAYA", "JUAN MAGAN", "BADALONA");
		imm = new Immigrant("123456789123", nac, "MOHA", "C/TANGER", "BADALONA");
		pr = new PermisResidencia(25, amf, imm, data, "23", "C/PLAYA", "AVI");

		assertEquals("C/PLAYA", pr.getDomicili());
	}

	@Test
	public void testsetNumpermis() throws Exception {
		// Comprovem que el set canvia el valor correctament.

		con = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", con);
		nac = new Nacio("ESP", "Espanya", zg);
		amf = new Amfitrio("00000000T", "C/PLAYA", "JUAN MAGAN", "BADALONA");
		imm = new Immigrant("123456789123", nac, "MOHA", "C/TANGER", "BADALONA");
		pr = new PermisResidencia(25, amf, imm, data, "23", "C/PLAYA", "AVI");

		pr.setNumPermis(99);
		assertEquals(99, pr.getNumPermis());
	}

	@Test
	public void testsetDataConcessio() throws Exception {
		// Comprovem que el set canvia el valor correctament.

		con = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", con);
		nac = new Nacio("ESP", "Espanya", zg);
		amf = new Amfitrio("00000000T", "C/PLAYA", "JUAN MAGAN", "BADALONA");
		imm = new Immigrant("123456789123", nac, "MOHA", "C/TANGER", "BADALONA");
		pr = new PermisResidencia(25, amf, imm, data, "23", "C/PLAYA", "AVI");

		assertEquals(data, pr.getDataConcessio());
	}

	@Test
	public void testsetDuradaConcessio() throws Exception {
		// Comprovem que el set canvia el valor correctament.

		con = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", con);
		nac = new Nacio("ESP", "Espanya", zg);
		amf = new Amfitrio("00000000T", "C/PLAYA", "JUAN MAGAN", "BADALONA");
		imm = new Immigrant("123456789123", nac, "MOHA", "C/TANGER", "BADALONA");
		pr = new PermisResidencia(25, amf, imm, data, "23", "C/PLAYA", "AVI");
		pr.setDuradaConcessio("29");
		assertEquals(29, pr.getDuradaConcessio());
	}

	@Test
	public void testsetDomicili() throws Exception {
		// Comprovem que el set canvia el valor correctament.

		con = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", con);
		nac = new Nacio("ESP", "Espanya", zg);
		amf = new Amfitrio("00000000T", "C/PLAYA", "JUAN MAGAN", "BADALONA");
		imm = new Immigrant("123456789123", nac, "MOHA", "C/TANGER", "BADALONA");
		pr = new PermisResidencia(25, amf, imm, data, "23", "C/PLAYA", "AVI");
		pr.setDomicili("CONTAINERS");
		assertEquals("CONTAINERS", pr.getDomicili());

	}

}
