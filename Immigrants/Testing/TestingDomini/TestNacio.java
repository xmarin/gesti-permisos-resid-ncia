package TestingDomini;

import static org.junit.Assert.*;

import org.junit.Test;

import Domini.Continent;
import Domini.Immigrant;
import Domini.Nacio;
import Domini.ZonaGeografica;

public class TestNacio {

	private Nacio nc;
	private ZonaGeografica zg;
	private Continent cont;

	@Test
	public void testCodiNacioNull() {
		// Comprovem que el codi nacio es null

		try {
			cont = new Continent("Europa");
			zg = new ZonaGeografica("Europa Oriental", cont);

			nc = new Nacio(null, "Espanya", zg);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testCodiNacioLlargaria() {
		// Comprovem que el codi nacio sobrepassa la llargaria max de la BD.

		try {
			cont = new Continent("Europa");
			zg = new ZonaGeografica("Europa Oriental", cont);

			nc = new Nacio("ESPP", "Espanya", zg);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testNomNull() {
		// Comprovem que el nom de la nacio es null

		try {
			cont = new Continent("Europa");
			zg = new ZonaGeografica("Europa Oriental", cont);

			nc = new Nacio("ESP", null, zg);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testNomLlargaria() {
		// Comprovem que el nom de la nacio sobrepassa la llargaria max de la
		// BD.

		try {
			cont = new Continent("Europa");
			zg = new ZonaGeografica("Europa Oriental", cont);

			nc = new Nacio("ESP", "AAAAAAAAAABBBBBBBBBBCCCCCCCCCCDDDDDDDDDDE",
					zg);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testZonaGeograficaNull() {
		// Comprovem que la Zona Geografica es null

		try {
			nc = new Nacio("ESP", "ESPANYA", null);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testgetCodiNacio() throws Exception {
		// Comprovem que el get recupera el valor correctament.
		cont = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", cont);
		nc = new Nacio("ESP", "ESPANYA", zg);
		assertEquals("ESP", nc.getCodiNacio());
	}

	@Test
	public void testgetNom() throws Exception {
		// Comprovem que el get recupera el valor correctament.
		cont = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", cont);
		nc = new Nacio("ESP", "ESPANYA", zg);
		assertEquals("ESPANYA", nc.getNom());
	}

	@Test
	public void testgetZonaGeografica() throws Exception {
		// Comprovem que el get recupera el valor correctament.
		cont = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", cont);
		nc = new Nacio("ESP", "ESPANYA", zg);

		assertEquals("Europa Oriental", zg.getNomZona());
	}

	@Test
	public void testsetCodiNacio() throws Exception {
		// Comprovem que el set canvia el valor correctament.
		cont = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", cont);
		nc = new Nacio("ESP", "ESPANYA", zg);
		nc.setCodiNacio("AND");

		assertEquals("AND", nc.getCodiNacio());
	}

	@Test
	public void testsetNom() throws Exception {
		// Comprovem que el set canvia el valor correctament.
		cont = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", cont);
		nc = new Nacio("ESP", "ESPANYA", zg);
		nc.setNom("ANDORRA");

		assertEquals("ANDORRA", nc.getNom());
	}

	@Test
	public void testsetZonaGeografica() throws Exception {
		// Comprovem que el set canvia el valor correctament.
		cont = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", cont);
		nc = new Nacio("ESP", "ESPANYA", zg);
		zg.setNomZona("CONTAINERS");

		assertEquals("CONTAINERS", zg.getNomZona());
	}

}
