package TestingDomini;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import Domini.Continent;
import Domini.Parentiu;
import Domini.ZonaGeografica;

public class TestZonaGeografica {

	private ZonaGeografica zg;
	private Continent cont;

	@Test
	public void testNomZonaGeograficaNull() {
		// Comprovem que el nom del continent es null

		try {
			cont = new Continent("Europa");

			zg = new ZonaGeografica(null, cont);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testNomZonaGeograficaLlargaria() {
		// Comprovem que el nom del continent sobrepassa la llargaria max de la
		// BD.

		try {
			cont = new Continent("Europa");

			zg = new ZonaGeografica(
					"AAAAAAAAAABBBBBBBBBBCCCCCCCCCCDDDDDDDDDDE", cont);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testContinentNull() {
		// Comprovem que el continent es null

		try {
			zg = new ZonaGeografica("Europa Oriental", null);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testgetNomZona() throws Exception {
		// Comprovem que el get recupera el valor correctament.
		cont = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", cont);
		assertEquals("Europa Oriental", zg.getNomZona());
	}

	@Test
	public void testgetContinent() throws Exception {
		// Comprovem que el get recupera el valor correctament.
		cont = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", cont);
		assertEquals(cont, zg.getContinent());
	}

	@Test
	public void testsetNomZona() throws Exception {
		// Comprovem que el set canvia el valor correctament.
		cont = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", cont);
		zg.setNomZona("Europa Occidental");
		assertEquals("Europa Occidental", zg.getNomZona());
	}

	@Test
	public void testsetContinent() throws Exception {
		// Comprovem que el set canvia el valor correctament.
		cont = new Continent("Asia");
		zg = new ZonaGeografica("Europa Oriental", cont);
		Continent cont2 = new Continent("Europa");
		zg.setContinent(cont2);
		assertEquals(cont2, zg.getContinent());
	}

}
