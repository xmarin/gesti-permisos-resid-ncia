package TestingDomini;

import static org.junit.Assert.*;

import org.junit.Test;

import Domini.Amfitrio;

public class TestAmfitrio {

	private Amfitrio amf;

	@Test
	public void testNifNull() {
		// Comprovem que el nif es null
		try {
			amf = new Amfitrio(null, "C/PLAYA", "JUAN PITO", "BADALONA");
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testNifIncorrecte() {
		// Comprovem que el nif es incorrecte
		try {
			amf = new Amfitrio("00000000c", "C/PLAYA", "JUAN PITO", "BADALONA");
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testDomiciliNull() {
		// Comprovem que el domicili es null
		try {
			amf = new Amfitrio("00000000t", null, "JUAN PITO", "BADALONA");
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testDomiciliLlargaria() {
		// Comprovem que el domicili sobrepassa la llargaria max de la BD.
		try {
			amf = new Amfitrio("00000000t",
					"AAAAAAAAAABBBBBBBBBBCCCCCCCCCCDDDDDDDDDDE", "JUAN PITO",
					"BADALONA");
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testFiliacioNull() {
		// Comprovem que la filiacio es null
		try {
			amf = new Amfitrio("00000000t", "C/PLAYA", null, "BADALONA");
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testFiliacioLlargaria() {
		// Comprovem que la filiacio sobrepassa la llargaria max de la BD.
		try {
			amf = new Amfitrio("00000000t", "C/PLAYA",
					"AAAAAAAAAABBBBBBBBBBCCCCCCCCCCDDDDDDDDDDE", "BADALONA");
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testPoblacioNull() {
		// Comprovem que la poblaci� es null
		try {
			amf = new Amfitrio("00000000t", "C/PLAYA", "JUAN PITO", null);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testPoblacioLlargaria() {
		// Comprovem que la poblaci� sobrepassa la llargaria max de la BD.
		try {
			amf = new Amfitrio("00000000t", "C/PLAYA", "JUAN PITO",
					"AAAAAAAAAABBBBBBBBBBCCCCCCCCCCDDDDDDDDDDE");
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testgetNif() throws Exception {
		// Comprovem que el get recupera el valor correctament.

		Amfitrio amf = new Amfitrio("47278336L", "C/PLA", "ADRI", "BADALONA");
		assertEquals("47278336L", amf.getNif());
	}

	@Test
	public void testsetNif() throws Exception {
		// Comprovem que el set canvia el valor correctament.

		Amfitrio amf = new Amfitrio("47278336L", "C/PLA", "ADRI", "BADALONA");

		amf.setNif("00000000T");
		assertEquals("00000000T", amf.getNif());

	}

}
