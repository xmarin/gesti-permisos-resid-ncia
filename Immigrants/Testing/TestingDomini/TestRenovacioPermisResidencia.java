package TestingDomini;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

import Domini.RenovacioPermisResidencia;

public class TestRenovacioPermisResidencia {

	private RenovacioPermisResidencia rpr;
	private Date data = new Date();

	@Test
	public void numPermisNumeroPositiu() {
		// comprovem que el numero de permis es < 0

		try {
			rpr = new RenovacioPermisResidencia(-8, data, "12", "C/PLAYA");
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void dataNull() {
		// comprovem que la data es null
		try {
			rpr = new RenovacioPermisResidencia(8, null, "12", "C/PLAYA");
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void duradaConcessioNull() {
		// comprovem que la durada de la concessio es null
		try {
			rpr = new RenovacioPermisResidencia(8, data, null, "C/PLAYA");
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void duradaConcessioLletra() {
		// comprovem que la durada de la concessio es una lletra
		try {
			rpr = new RenovacioPermisResidencia(8, data, "r", "C/PLAYA");
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void duradaConcessioNumeroPositiu() {
		// comprovem que la durada de la concessio es < 0
		try {
			rpr = new RenovacioPermisResidencia(8, data, "-1", "C/PLAYA");
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void domiciliNull() {
		// comprovem que el domicili es null
		try {
			rpr = new RenovacioPermisResidencia(8, data, "12", null);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void domiciliLlargaria() {
		// comprovem que el domicili sobrepassa la llargaria max de la BD.
		try {
			rpr = new RenovacioPermisResidencia(8, data, "12",
					"AAAAAAAAAABBBBBBBBBBCCCCCCCCCCDDDDDDDDDDE");
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testgetNumPermis() throws Exception {
		// Comprovem que el get recupera el valor correctament.

		rpr = new RenovacioPermisResidencia(8, data, "12", "C/PLAYA");
		assertEquals(8, rpr.getNumPermis());
	}

	@Test
	public void testgetDuradaRenovacio() throws Exception {
		// Comprovem que el get recupera el valor correctament.

		rpr = new RenovacioPermisResidencia(8, data, "12", "C/PLAYA");
		assertEquals(12, rpr.getDuradaRenovacio());
	}

	@Test
	public void testgetDataRenovacio() throws Exception {
		// Comprovem que el get recupera el valor correctament.

		rpr = new RenovacioPermisResidencia(8, data, "12", "C/PLAYA");
		assertEquals(data, rpr.getDataRenovacio());
	}

	@Test
	public void testgetDomicili() throws Exception {
		// Comprovem que el get recupera el valor correctament.

		rpr = new RenovacioPermisResidencia(8, data, "12", "C/PLAYA");
		assertEquals("C/PLAYA", rpr.getDomicili());
	}

	@Test
	public void testsetNumPermis() throws Exception {
		// Comprovem que el set canvia el valor correctament.

		rpr = new RenovacioPermisResidencia(8, data, "12", "C/PLAYA");
		rpr.setNumPermis(9);
		assertEquals(9, rpr.getNumPermis());
	}

	@Test
	public void testsetDuradaRenovacio() throws Exception {
		// Comprovem que el set canvia el valor correctament.

		rpr = new RenovacioPermisResidencia(8, data, "12", "C/PLAYA");
		rpr.setDuradaRenovacio("98");
		assertEquals(98, rpr.getDuradaRenovacio());
	}

	@Test
	public void testsetDomicili() throws Exception {
		// Comprovem que el set canvia el valor correctament.

		rpr = new RenovacioPermisResidencia(8, data, "12", "C/PLAYA");
		rpr.setDomicili("CONTAINER");
		assertEquals("CONTAINER", rpr.getDomicili());
	}

}
