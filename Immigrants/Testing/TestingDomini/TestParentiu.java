package TestingDomini;

import Domini.Parentiu;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

public class TestParentiu {

	private Parentiu par;

	@Test
	public void testParentiuNull() {
		// Comprovem que el parentiu es null
		try {
			par = new Parentiu(null);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testParentiuLlargaria() {
		// Comprovem que el parentiu sobrepassa la llargaria max de la BD.
		try {
			par = new Parentiu("AAAAAAAAAAB");
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testgetNom() throws Exception {
		// Comprovem que el get recupera el valor correctament.
		par = new Parentiu("EX");
		assertEquals("EX", par.getTipus());
	}

	@Test
	public void testsetTipus() throws Exception {
		// Comprovem que el set canvia el valor correctament.
		par = new Parentiu("EX");
		par.setTipus("AMIC");
		assertEquals("AMIC", par.getTipus());
	}

}
