package TestingAplicacio;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Aplicacio.ControladorGestioAmfitrio;

public class TestControladorGestioAmfitrio {

	private ControladorGestioAmfitrio ga;

	@Before
	public void setUp() throws Exception {
		ga = new ControladorGestioAmfitrio();
	}

	@Test
	public void testExisteixAmfitrioNull() {
		// comprovem que al buscar un amfitrio amb nif null salta l'excepci�

		try {
			ga.existeixAmfitrio(null);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testgetAmfitrioRepresentationNull() {

		try {
			ga.getAmfitrioRepresentation(null);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

}
