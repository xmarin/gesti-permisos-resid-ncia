package TestingAplicacio;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Aplicacio.ControladorHistorialImmigrant;

public class TestControladorHistorialImmigrant {

	private ControladorHistorialImmigrant hi;

	@Before
	public void setUp() throws Exception {
		hi = new ControladorHistorialImmigrant();
	}

	@Test
	public void testsearchImmigrantPassaportOrNameNull() {
		// comprovem que al buscar un historial d'un immigrant
		// amb passaport/nom null salta l'excepci�

		try {
			hi.searchImmigrant(null);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testcarregaImmigrantPassaportNull() {
		// comprovem que al carregar un immigrant amb passaport null salta
		// l'excepci�

		try {
			hi.carregaImmigrant(null);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testgetNacioNull() {
		// comprovem que al fer get d'una naci� amb codi null salta l'excepci�

		try {
			hi.getNacio(null);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

}
