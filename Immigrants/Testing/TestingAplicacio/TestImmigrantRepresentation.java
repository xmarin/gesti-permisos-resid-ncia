package TestingAplicacio;

import static org.junit.Assert.*;

import org.junit.Test;

import Aplicacio.Representations.AmfitrioRepresentation;
import Aplicacio.Representations.ImmigrantRepresentation;
import Domini.Amfitrio;
import Domini.Continent;
import Domini.Immigrant;
import Domini.Nacio;
import Domini.ZonaGeografica;

public class TestImmigrantRepresentation {

	private ImmigrantRepresentation ir;
	private Immigrant imm = null;
	private Continent cont;
	private ZonaGeografica zg;
	private Nacio nac;

	@Test
	public void testAmfitrioNull() {
		// Comprovem que el immigrant es null
		try {
			ir = new ImmigrantRepresentation(imm);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testgetPoblacio() throws Exception {
		// Comprovem que el get recupera el valor correctament.

		cont = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", cont);
		nac = new Nacio("ESP", "ESPANYA", zg);
		imm = new Immigrant("123456789123", nac, "JUAN PITO", "C/PLAYA",
				"BADALONA");

		ir = new ImmigrantRepresentation(imm);
		assertEquals("BADALONA", imm.getPoblacio());
	}

	@Test
	public void testgetPassaport() throws Exception {
		// Comprovem que el get recupera el valor correctament.

		cont = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", cont);
		nac = new Nacio("ESP", "ESPANYA", zg);
		imm = new Immigrant("123456789123", nac, "JUAN PITO", "C/PLAYA",
				"BADALONA");

		ir = new ImmigrantRepresentation(imm);
		assertEquals("123456789123", imm.getPassaport());
	}

	@Test
	public void testgetFiliacio() throws Exception {
		// Comprovem que el get recupera el valor correctament.

		cont = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", cont);
		nac = new Nacio("ESP", "ESPANYA", zg);
		imm = new Immigrant("123456789123", nac, "JUAN PITO", "C/PLAYA",
				"BADALONA");

		ir = new ImmigrantRepresentation(imm);
		assertEquals("JUAN PITO", imm.getFiliacio());
	}

	@Test
	public void testgetDomicili() throws Exception {
		// Comprovem que el get recupera el valor correctament.

		cont = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", cont);
		nac = new Nacio("ESP", "ESPANYA", zg);
		imm = new Immigrant("123456789123", nac, "JUAN PITO", "C/PLAYA",
				"BADALONA");

		ir = new ImmigrantRepresentation(imm);
		assertEquals("C/PLAYA", imm.getDomicili());
	}

	@Test
	public void testgetCodiNacio() throws Exception {
		// Comprovem que el get recupera el valor correctament.

		cont = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", cont);
		nac = new Nacio("ESP", "ESPANYA", zg);
		imm = new Immigrant("123456789123", nac, "JUAN PITO", "C/PLAYA",
				"BADALONA");

		ir = new ImmigrantRepresentation(imm);
		assertEquals("ESP", nac.getCodiNacio());
	}

	@Test
	public void testgetNacio() throws Exception {
		// Comprovem que el get recupera el valor correctament.

		cont = new Continent("Europa");
		zg = new ZonaGeografica("Europa Oriental", cont);
		nac = new Nacio("ESP", "ESPANYA", zg);
		imm = new Immigrant("123456789123", nac, "JUAN PITO", "C/PLAYA",
				"BADALONA");
		ir = new ImmigrantRepresentation(imm);
		assertEquals("ESPANYA", nac.getNom());
	}

}
