package TestingAplicacio;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Aplicacio.ControladorHistorialAmfitrio;
import Aplicacio.Representations.AmfitrioRepresentation;
import Domini.Amfitrio;

public class TestControladorHistorialAmfitrio {

	private ControladorHistorialAmfitrio ha;

	@Before
	public void setUp() throws Exception {
		ha = new ControladorHistorialAmfitrio();
	}

	@Test
	public void testsearcgAmfitrioNifOrNameNull() {
		// comprovem que al buscar un amfitrio amb nif o name null salta
		// l'excepci�

		try {
			ha.searchAmfitrio(null);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testcarregaAmfitrioPassaportNull() {
		// comprovem que al carregar un amfitrio amb passaport null salta
		// l'excepci�

		try {
			ha.carregaAmfitrio(null);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

}
