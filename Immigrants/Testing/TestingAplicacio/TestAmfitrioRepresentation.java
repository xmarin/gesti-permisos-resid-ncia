package TestingAplicacio;

import static org.junit.Assert.*;

import org.junit.Test;

import Aplicacio.Representations.AmfitrioRepresentation;
import Domini.Amfitrio;
import Domini.Continent;
import Domini.Nacio;
import Domini.ZonaGeografica;

public class TestAmfitrioRepresentation {

	private AmfitrioRepresentation ar;
	private Amfitrio am = null;

	@Test
	public void testAmfitrioNull() {
		// Comprovem que l'amfitrio es null
		try {
			ar = new AmfitrioRepresentation(am);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testgetNif() throws Exception {
		// Comprovem que el get recupera el valor correctament.

		am = new Amfitrio("00000000T", "C/PLAYA", "PACO", "BADALONA");
		ar = new AmfitrioRepresentation(am);
		assertEquals("00000000T", ar.getNif());
	}

	@Test
	public void testgetDomicili() throws Exception {
		// Comprovem que el get recupera el valor correctament.

		am = new Amfitrio("00000000T", "C/PLAYA", "PACO", "BADALONA");
		ar = new AmfitrioRepresentation(am);
		assertEquals("C/PLAYA", ar.getDomicili());
	}

	@Test
	public void testgetFiliacio() throws Exception {
		// Comprovem que el get recupera el valor correctament.

		am = new Amfitrio("00000000T", "C/PLAYA", "PACO", "BADALONA");
		ar = new AmfitrioRepresentation(am);
		assertEquals("PACO", ar.getFiliacio());
	}

	@Test
	public void testgetPoblacio() throws Exception {
		// Comprovem que el get recupera el valor correctament.

		am = new Amfitrio("00000000T", "C/PLAYA", "PACO", "BADALONA");
		ar = new AmfitrioRepresentation(am);
		assertEquals("BADALONA", ar.getPoblacio());
	}

}
