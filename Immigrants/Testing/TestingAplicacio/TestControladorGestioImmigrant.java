package TestingAplicacio;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Aplicacio.ControladorGestioImmigrant;

public class TestControladorGestioImmigrant {

	private ControladorGestioImmigrant cgi;

	@Before
	public void setUp() throws Exception {
		cgi = new ControladorGestioImmigrant();
	}

	@Test
	public void testcarregaImmigrantPassaportNull() {
		// comprovem que al carregar un immigrant amb passaport null salta
		// l'excepci�

		try {
			cgi.carregaImmigrant(null);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testImmigrantRepresentationNull() {

		try {
			cgi.getImmigrantRepresentation(null);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

}
