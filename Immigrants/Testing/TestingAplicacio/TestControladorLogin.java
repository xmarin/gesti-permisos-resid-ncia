package TestingAplicacio;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Aplicacio.ControladorLogin;

;

public class TestControladorLogin {

	private ControladorLogin cl;

	@Before
	public void setUp() throws Exception {
		cl = new ControladorLogin();
	}

	@Test
	public void testLoginUserPassNull() {
		// comprovem que al inserir un usuari i una contrasenya null salta
		// l'excepci�

		String user = null;
		char[] pass = null;

		try {
			cl.login(user, pass);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testLoginPassNull() {
		// comprovem que al inserir una contrasenya null salta l'excepci�

		String user = "admin";
		char[] pass = null;

		try {
			cl.login(user, pass);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testLoginUserNull() {
		// comprovem que al inserir un usuari null salta l'excepci�

		String user = null;
		String pass = "admin";
		char pwd[];
		pwd = pass.toCharArray();

		try {
			cl.login(user, pwd);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

}
