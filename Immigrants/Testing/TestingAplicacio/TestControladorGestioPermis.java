package TestingAplicacio;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Aplicacio.ControladorGestioPermis;

public class TestControladorGestioPermis {

	private ControladorGestioPermis gp;

	@Before
	public void setUp() throws Exception {
		gp = new ControladorGestioPermis();
	}

	@Test
	public void testCarregaAmfitrio() {
		// comprovem que al carregar un amfitrio null salta l'excepci�

		try {
			gp.carregaAmfitrio(null);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testCarregaImmigrant() {
		// comprovem que al carregar un immigrant null salta l'excepci�

		try {
			gp.carregaImmigrant(null);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testCarregaPermis() {
		// comprovem que al carregar un permis null salta l'excepci�

		try {
			gp.carregaPermis(null);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testCarregaRenovacio() {
		// comprovem que al carregar una renovacio null salta l'excepci�

		try {
			gp.carregaRenovacio(null);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

	@Test
	public void testgetRenovacioRepresentation() {

		try {
			gp.getRenovacioRepresentation(null);
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}

}
